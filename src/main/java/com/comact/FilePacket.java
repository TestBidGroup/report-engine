/*
 * FilePacket.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact;

import com.comact.io.StreamTools;
import org.apache.commons.io.IOUtils;
import org.springframework.util.StreamUtils;

import java.io.*;

/**
 * Class to represent a File object that can be sent
 * and recreated on another system by serialisation or RMI
 * Will actually do the creating for you
 *
 * @author pymathieu
 * @version 1.0, 2005-03-11 (11:47:03)
 * @since 1.4
 */
@SuppressWarnings("serial")
public class FilePacket implements Serializable, Comparable<FilePacket> {

    /**
     * this is the client File object
     */
    private String fileName;
    /**
     * the file data
     */
    private byte[] data;

    /**
     * Make a file packet that represents a given filename
     *
     * @param clientFileName The filename this represents
     */
    public FilePacket(String clientFileName) throws Exception {
        this(new File(clientFileName));
    }

    /**
     * Make a file packet that represents a given filename
     * After construction the file packet will constaint the data of the client file
     *
     * @param clientFile The filename this represents
     */
    public FilePacket(String filename, File clientFile) throws Exception {
        this(clientFile);
        setFileName(filename);
    }

    /**
     * Make a file packet that represents a given filename
     * After construction the file packet will constaint the data of the client file
     *
     * @param clientFile The filename this represents
     */
    public FilePacket(File clientFile) throws Exception {
        if (clientFile == null) {
            throw new IllegalArgumentException("Argument clientFile cant be null !");
        }
        if (clientFile.isDirectory() == true) {
            throw new IllegalArgumentException("Argument clientFile cant be a directory !");
        }

        if (clientFile.length() > Integer.MAX_VALUE) {
            throw new IllegalArgumentException("Client File is to big to be sent by serialization ! Max file size accepted is " + Integer.MAX_VALUE + " byte.");
        }
        setFileName(clientFile.getName());

        byte[] tmp = new byte[(int) (clientFile.length())];
        FileInputStream fileInputStream = new FileInputStream(clientFile);
        fileInputStream.read(tmp);
        fileInputStream.close();
        setData(tmp);
    }

    /**
     * Make a file packet from the input stream.
     *
     * @param inputStream The stream which will read the data.
     * @throws IOException If the stream cannot be read.
     */
    public FilePacket(InputStream inputStream) throws IOException {
        int size = inputStream.available();
        byte[] tmp = new byte[size];
        inputStream.read(tmp);
        setData(tmp);
    }

    public FilePacket(String name, byte[] data) {
        setFileName(name);
        setData(data);
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Have the file packet recreate itself, <b>used
     * after sending it to another location </b>.
     * File will have same name and contents
     *
     * @param fileName The file name to write itself to
     */
    public void writeTo(String fileName) throws Exception {
        writeTo(new File(fileName));
    }

    /**
     * Have the file packet recreate itself, <b>used
     * after sending it to another location </b>.
     * File will have same name and contents
     *
     * @param fileOut The file to write itself to
     */
    public void writeTo(File fileOut) throws Exception {
        FileOutputStream outputStream = new FileOutputStream(fileOut);
        writeTo(outputStream);
        StreamTools.closeOutputStream(outputStream);
    }

    /**
     * Have the file packet recreate itself, <b>used
     * after sending it to another location </b>.
     * File will have same name and contents
     *
     * @param output The outputStream to write itself to
     */
    public void writeTo(OutputStream output) throws Exception {
        if(data != null) {
            output.write(data);
        }
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public int compareTo(FilePacket o) {
        return getFileName().compareTo(o.getFileName());
    }

    @Override
    public String toString() {
        return fileName;
    }
}
