/*
 * BirtTreeCascadingParameter.java
 *
 * Copyright (c) 2013 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.comact;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * User: dgauthier
 * Date: 13-11-01
 */
public class BirtTreeCascadingParameter {

    private BirtNodeCascadingParameter root;

    public BirtTreeCascadingParameter() {
        root = new BirtNodeCascadingParameter();
        root.setValue(null);
        root.setParent(null);
        root.setChildren(new ArrayList<>());
    }

    public BirtNodeCascadingParameter getRoot() {
        return root;
    }

    public Collection<BirtNodeCascadingParameter> getNodesByParamName(String paramName) {
        Collection<BirtNodeCascadingParameter> ret = new ArrayList<>();
        filterNodeByParamName(paramName, root, ret);
        return ret;
    }

    private void filterNodeByParamName(String paramName, BirtNodeCascadingParameter node, Collection<BirtNodeCascadingParameter> birtNodeCascadingParameters) {
        if (node != null) {
            if (Objects.equals(node.getParamName(), paramName)) {
                birtNodeCascadingParameters.add(node);
            }

            for (BirtNodeCascadingParameter child : node.children) {
                filterNodeByParamName(paramName, child, birtNodeCascadingParameters);
            }
        }
    }

    public static class BirtNodeCascadingParameter {
        private Object value;
        private String tagName;
        private String paramName;
        private BirtNodeCascadingParameter parent;
        private Collection<BirtNodeCascadingParameter> children;

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public String getParamName() {
            return paramName;
        }

        public void setParamName(String paramName) {
            this.paramName = paramName;
        }

        public BirtNodeCascadingParameter getParent() {
            return parent;
        }

        public void setParent(BirtNodeCascadingParameter parent) {
            this.parent = parent;
        }

        public Collection<BirtNodeCascadingParameter> getChildren() {
            return children;
        }

        public void setChildren(Collection<BirtNodeCascadingParameter> children) {
            this.children = children;
        }

        public String getTagName() {
            return tagName;
        }

        public void setTagName(String tagName) {
            this.tagName = tagName;
        }
    }
}
