package com.comact;

/**
 * User: majoly
 * Date: 13-12-18
 */
public enum CurrencySymbol {

    DOLLAR(new String("$")),
    REAL(new String("R$")),
    EURO(new String("€"));

    private String name;

    private CurrencySymbol(String name) {
        this.name = name;
    }

    public String currencyName() {
        return name;
    }

    public String toString() {
        return name;
    }
}
