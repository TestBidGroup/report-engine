package com.comact.report.controller;

import com.comact.beans.DynamicDataBean;
import com.comact.report.bean.ReportParameterV1;
import com.comact.report.bean.ReportRequestBeanV1;
import com.comact.report.bean.ReportTemplateV1;
import com.comact.report.service.BirtReportService;
import com.comact.rest.argument.DisplayContext;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * User: dlauzon
 * Date: 2016-12-15
 * Time: 10:34
 */
@Controller
@RequestMapping(value = {"/api/v1/templates"})
public class ReportController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class.getSimpleName());
    @Autowired
    private BirtReportService birtService;

    @RequestMapping(method = RequestMethod.GET, produces =  "application/json")
    public ResponseEntity<List<ReportTemplateV1>> list() {
        List<ReportTemplateV1> ret = new ArrayList<>();
        String[] templates = birtService.getReportList();

        for (String template : templates) {
            ReportTemplateV1 templateV1 = new ReportTemplateV1();
            templateV1.setId(template);

            Collection<ReportParameterV1> parameters = birtService.getReportParameters(template);
            templateV1.setParameters(parameters);

            ret.add(templateV1);
        }

        return new ResponseEntity<>(ret, HttpStatus.OK);
    }
    @RequestMapping(value = {"/{template}"}, method = RequestMethod.GET, produces =  "application/json")
    public ResponseEntity<ReportTemplateV1> get(@PathVariable String template) {
        String[] templates = birtService.getReportList();
        if(!Arrays.asList(templates).contains(template))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        ReportTemplateV1 templateV1 = new ReportTemplateV1();
        templateV1.setId(template);

        Collection<ReportParameterV1> parameters = birtService.getReportParameters(template);
        templateV1.setParameters(parameters);

        return new ResponseEntity<>(templateV1, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, produces =  {"application/pdf","application/vnd.ms-excel"})
    public ResponseEntity<byte[]> getReport(@RequestBody ReportRequestBeanV1 requestBean, DisplayContext context) throws Exception {
        long token = birtService.submit(requestBean);
        return new ResponseEntity<>(birtService.get(token), HttpStatus.OK);
    }

    @RequestMapping(value = {"/actions"}, method = RequestMethod.POST)
    public ResponseEntity<Long> submit(@RequestBody ReportRequestBeanV1 requestBean, DisplayContext context){
        return new ResponseEntity<>(birtService.submit(requestBean), HttpStatus.OK);
    }
    @RequestMapping(value = {"/actions/{token}/status"}, method = RequestMethod.GET, produces =  "application/json")
    public ResponseEntity<DynamicDataBean> getStatus(@PathVariable long token){
        DynamicDataBean reply = new DynamicDataBean();
        reply.set("done", birtService.isDone(token));
        return new ResponseEntity<>(reply, HttpStatus.OK);
    }
    @RequestMapping(value = {"/actions/{token}"}, method = RequestMethod.DELETE)
    public ResponseEntity cancel(@PathVariable long token){
        birtService.cancel(token);
        return new ResponseEntity(HttpStatus.OK);
    }
    @RequestMapping(value = {"/actions/{token}"}, method = RequestMethod.GET, produces =  "application/pdf")
    public ResponseEntity<byte[]> getReply(@PathVariable long token) throws Exception {
        byte[] bytes = birtService.get(token);
        return new ResponseEntity<>(bytes, HttpStatus.OK);
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<DynamicDataBean> error(Exception exception){
        LOGGER.error(exception.getMessage(), exception);
        DynamicDataBean error = new DynamicDataBean();
        error.set("message", exception.getMessage());
        String stack = ExceptionUtils.getStackTrace(exception);
        error.set("stack", stack);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
