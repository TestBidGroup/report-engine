/*
 * StringValidator.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.validators;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-01-08 (14:44:37)
 * @since 1.4
 */
public class StringValidator extends DefaultObjectValidator<String> {

    protected final static boolean ACCEPT_EMPTY_STRING_BY_DEFAULT = true;

    private boolean acceptEmptyString;

    public StringValidator(boolean acceptNullString, String defaultValidString) throws ObjectValidatorException {
        this(acceptNullString, ACCEPT_EMPTY_STRING_BY_DEFAULT, defaultValidString);
    }

    public StringValidator(boolean acceptNullString, boolean acceptEmptyString, String defaultValidString) throws ObjectValidatorException {
        super(acceptNullString);

        this.acceptEmptyString = acceptEmptyString;

        setDefaultValidObject(defaultValidString);
    }

    public void validateObject(String aString) throws ObjectValidatorException {
        super.validateObject(aString);

        if ((aString != null) && (acceptEmptyString == false) && (aString.length() < 1)) {
            throw new ObjectValidatorException("The string must be a non empty one.");
        }
    }

    /**
     * Vérification qu'un chaîne de caractère est vide ou nulle.
     *
     * @param aString La chaîne à valider.
     * @return true si nulle ou vide, sinon false.
     */
    public static boolean isNullOrEmpty(final String aString) {
        return aString == null || aString.trim().length() == 0;
    }

    /**
     * Vérification qu'une chaîne de caractère n'est pas plus ou moins longue que les paramètres spécifiés.
     *
     * @param aString   La chaîne de caractère à valider.
     * @param minLength La longueur minumum que la chaîne devrait avoir.
     * @param maxLength La longueur maximum que la chaîne devrait avoir.
     * @return true si la longueur de la chaîne respecte les critères.
     */
    public static boolean isInRange(final String aString, final int minLength, final int maxLength) {
        boolean isInRange = true;

        if (minLength > 0 && aString == null) {
            isInRange = false;
        }

        if (aString != null) {
            int strLength = aString.length();
            isInRange = strLength >= minLength && strLength <= maxLength;
        }

        return isInRange;
    }
}
