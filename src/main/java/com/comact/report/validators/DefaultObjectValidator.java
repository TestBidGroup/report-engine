/*
 * DefaultObjectValidator.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.validators;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-04-05 (16:12:38)
 * @since 1.4
 */
public class DefaultObjectValidator <O> extends AbstractObjectValidator<O>
{

    public DefaultObjectValidator()
    {
        this( ACCEPT_NULL_OBJECT_BY_DEFAULT );
    }

    public DefaultObjectValidator( boolean acceptNullObject )
    {
        super( acceptNullObject );
    }

    public DefaultObjectValidator( O defaultValidObject ) throws ObjectValidatorException
    {
        this( ACCEPT_NULL_OBJECT_BY_DEFAULT, defaultValidObject );
    }

    public DefaultObjectValidator( boolean acceptNullObject, O defaultValidObject ) throws ObjectValidatorException
    {
        super( acceptNullObject );

        setDefaultValidObject( defaultValidObject );
    }

}
