/*
 * MUnitValidator.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.validators;

import com.comact.lang.MUnit.MUnit;
import com.comact.lang.MUnit.MUnitType;

/**
 * Type the class description here.
 *
 * @author csavard
 * @version 1.0, 2005-04-19 (09:57:18)
 * @since 1.4
 */
public class MUnitValidator extends NonNullObjectValidator<MUnit>
{

    private MUnitType mUnitType;

    public MUnitValidator( MUnitType mUnitType )
    {
        this.mUnitType = mUnitType;
    }

    public void validateObject( MUnit mUnit ) throws ObjectValidatorException
    {
        super.validateObject( mUnit );

        if( mUnit.type().equals( mUnitType ) == false )
        {
            throw new ObjectValidatorException( "Invalid unit type: " + mUnit.type().name() + ". Unit type must be" + mUnitType.name() + "." );
        }
    }
}
