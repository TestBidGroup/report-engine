/*
 * NonNullObjectValidator.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.validators;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-04 (08:23:17)
 * @since 1.4
 */
public class NonNullObjectValidator <O> extends DefaultObjectValidator<O>
{

    public NonNullObjectValidator()
    {
        super( false );
    }

    public NonNullObjectValidator( O defaultValidObject ) throws ObjectValidatorException
    {
        super( false, defaultValidObject );
    }

}
