/*
 * AbstractObjectValidator.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-04-13 (15:12:55)
 * @since 1.4
 */
public abstract class AbstractObjectValidator <O> implements ObjectValidator<O>
{

    private transient Logger logger = null;

    protected static final boolean ACCEPT_NULL_OBJECT_BY_DEFAULT = false;

    private boolean acceptNullObject;

    private O defaultValidObject;

    protected AbstractObjectValidator( boolean acceptNullObject )
    {
        this.acceptNullObject = acceptNullObject;
    }

    public boolean isAcceptNullObject()
    {
        return acceptNullObject;
    }

    public final O getDefaultValidObject()
    {
        return defaultValidObject;
    }

    public final void setDefaultValidObject( O defaultValidObject ) throws ObjectValidatorException
    {
        validateObject( defaultValidObject );

        this.defaultValidObject = defaultValidObject;
    }

    public void validateObject( O anObject ) throws ObjectValidatorException
    {
        if( ( anObject == null ) && ( acceptNullObject == false ) )
        {
            throw new ObjectValidatorException( "Object can't be null", new NullPointerException() );
        }
    }

    protected final Logger logger()
    {
        if( logger == null )
        {
            logger = LoggerFactory.getLogger( getClass() );
        }
        return logger;
    }

    public Object clone() throws CloneNotSupportedException
    {
        AbstractObjectValidator clone = ( AbstractObjectValidator ) super.clone();

        if( defaultValidObject != null && defaultValidObject instanceof Cloneable )
        {
            try
            {
                clone.defaultValidObject = ( O ) defaultValidObject.getClass().getMethod( "clone", ( Class[] ) null ).invoke( defaultValidObject, ( Object[] ) null );
            }
            catch( IllegalAccessException e )
            {
                logger().error( null, e );
            }
            catch( InvocationTargetException e )
            {
                logger().error( null, e );
            }
            catch( NoSuchMethodException e )
            {
                logger().error( null, e );
            }
        }

        return clone;
    }
}
