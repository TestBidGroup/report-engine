/*
 * CalendarValidator.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.validators;

import java.util.Calendar;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-04-06 (08:38:35)
 * @since 1.4
 */
public class CalendarValidator extends DefaultObjectValidator<Calendar>
{

    public CalendarValidator()
    {
        this( ACCEPT_NULL_OBJECT_BY_DEFAULT );
    }

    public CalendarValidator( boolean acceptNullCalendar )
    {
        super( acceptNullCalendar );

        if( acceptNullCalendar == false )
        {
            try
            {
                setDefaultValidObject( Calendar.getInstance() );
            }
            catch( ObjectValidatorException e )
            {
                logger().error( null, e );
            }
        }
    }

    public CalendarValidator( Calendar defaultValidCalendar ) throws ObjectValidatorException
    {
        super( defaultValidCalendar );
    }

    public CalendarValidator( boolean acceptNullCalendar, Calendar defaultValidCalendar ) throws ObjectValidatorException
    {
        super( acceptNullCalendar, defaultValidCalendar );
    }

}
