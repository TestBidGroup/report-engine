/*
 * NonNullStringValidator.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.validators;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-01-08 (14:44:37)
 * @since 1.4
 */
public class NonNullStringValidator extends StringValidator
{

    public static final String DEFAULT_VALID_STRING = "";

    public NonNullStringValidator() throws ObjectValidatorException
    {
        this( DEFAULT_VALID_STRING );
    }

    public NonNullStringValidator( String defaultValidString ) throws ObjectValidatorException
    {
        super( false, true, defaultValidString );
    }

}
