/*
 * ObjectValidatorException.java
 *
 * Copyright (c) 2013 Comact Optimisation, Inc. All rights reserverd.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.comact.report.validators;

import com.comact.localization.LocalizedException;
import com.comact.localization.LocalizedString;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-04-05 (15:15:18)
 * @since 1.4
 */
public class ObjectValidatorException extends LocalizedException {

    public ObjectValidatorException() {
        super();
    }

    public ObjectValidatorException(Throwable cause) {
        super(cause);
    }

    public ObjectValidatorException(String message) {
        super(message);
    }

    public ObjectValidatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public ObjectValidatorException(LocalizedString localizedMessage) {
        super(localizedMessage);
    }

    public ObjectValidatorException(LocalizedString localizedMessage, Throwable cause) {
        super(localizedMessage, cause);
    }
}
