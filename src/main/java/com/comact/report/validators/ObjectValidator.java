/*
 * ObjectValidator.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.validators;

import java.io.Serializable;

/**
 * Type the interface description here.
 *
 * @author slacasse
 * @version 1.0, 2004-04-13 (09:18:07)
 * @since 1.4
 */
public interface ObjectValidator <O> extends Serializable, Cloneable
{

    public void validateObject(O object) throws ObjectValidatorException;

    public O getDefaultValidObject();

    public void setDefaultValidObject(O defaultValidObject) throws ObjectValidatorException;

    public Object clone() throws CloneNotSupportedException;
}
