/*
 * RTAReportParameter.java
 *
 * Copyright (c) 2015 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.comact.report.util;


import com.comact.language.Language;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

/**
 * User: dgauthier
 * Date: 14-05-14
 */
public enum RTAReportParameter {

    LANGUAGE("Language", true, false, Language.class),
    BEGIN_DATE("BeginDate", true, false, Calendar.class),
    END_DATE("EndDate", true, false, Calendar.class),
    MACHINE_NAME("MachineName", true, false, String.class),
    MACHINE_DISPLAY_NAME("MachineDisplayName", true, false, String.class),
    CUSTOMER_NAME("CustomerName", true, false, String.class),
    SHIFT_ID("ShiftId", true, false, Integer.class),
    TAG_ID("TagId", true, false, Integer.class),
    METRIC("Metric", true, false, Boolean.class),
    INPUT_METRIC("InputMetric", true, false, Boolean.class),
    INTERNATIONAL_VOLUME_MODE("InternationalVolumeMode", true, false, Boolean.class),
    MARKET_VOLUME_MODE("DisplayMarketVolume", true, false, Boolean.class),
    LARGE_SCALE_COEFFICIENT("LargeScaleCoefficient", true, false, Boolean.class),
    PERIOD_TEXT("PeriodText", true, false, String.class),
    PARAMETER_SELECTION("ParameterSelection", true, false, String.class),

    SHIFT_NAME("ShiftName", true, false, String.class),
    TEAM_NAME("TeamName", true, false, String.class),
    TYPE_OPTI("TypeOpti", true, false, Integer.class),
    SQL_FRAGMENT("SqlFragment", true, false, String.class),
    COPYRIGHT_YEAR("CopyrightYear", true, false, Integer.class),
    TITLE("Title", true, false, String.class),
    BATCH_ID("BatchId", true, false, Collection.class),
    BATCH_DEFINITION_ID("BatchDefinitionId", true, false, String.class),
    BIN_NUMBER("BinNumber", true, true, Collection.class),
    PROVIDER("Provider", true, true, Collection.class),
    SCAN_MODE("ScanMode", false, true, Integer.class),
    SUB_MACHINE("SubMachine", false, true, Integer.class)
    ;
    //private static DoMetadata wellKnownParametersMetadata = null;
    private final String propertyName;
    private final boolean wellKnown;
    private final boolean printOnReport;
    private final Class clazz;

    RTAReportParameter(String propertyName, boolean wellKnown, boolean printOnReport, Class clazz) {

        this.propertyName = propertyName;
        this.wellKnown = wellKnown;
        this.printOnReport = printOnReport;
        this.clazz = clazz;
    }

    public static Collection<RTAReportParameter> getWellKnownParameters() {
        RTAReportParameter[] allParameters = RTAReportParameter.values();

        Collection<RTAReportParameter> wellKnownParameters = new ArrayList<RTAReportParameter>(allParameters.length);

        for (RTAReportParameter reportParameter : allParameters) {
            if (reportParameter.isWellKnown()) {
                wellKnownParameters.add(reportParameter);
            }
        }

        return wellKnownParameters;
    }

    /**
     * Méthode utilitaire qui détermine si le parametre est utilisable en vertu de "well Known" et des metadatas
     * @param name nom du parametre
     * @return si
     */
    public static synchronized boolean isDisplayable(String name){
        /*if (wellKnownParametersMetadata == null) {
            WellKnownParameters wellKnownParameters = new WellKnownParameters();
            wellKnownParametersMetadata = MetadataUtil.createMetadata(wellKnownParameters, false, true, MetadataVersionProvider.class);
        }

        DopMetadata dop = null;
        try {
           dop = wellKnownParametersMetadata.getDopMetadata(name);
        } catch (DataObjectPropertyException e){
            // Parametre pas d'override
        }*/
        RTAReportParameter prop = getByPropertyName(name);
        boolean displayable = prop == null || !prop.isWellKnown() || prop.isPrintOnReport();
        /*if (displayable && dop != null) {
            displayable = dop.getValueIsDisplayable();
        }*/
        return displayable;
    }

    /**
     * Permet de savoir si le nom du paramètre reçu en paramètre est un paramètre connu.
     *
     * @param parameterName Le nom du paramètre
     * @return true si le paramètre est connu.
     */
    public static boolean isWellKnow(String parameterName) {
        boolean ret = false;
        for (RTAReportParameter reportParameter : values()) {
            if (reportParameter.getPropertyName().equals(parameterName)) {
                ret = reportParameter.isWellKnown();
                break;
            }
        }

        return ret;
    }

    /**
     * Permet de retrouver un paramètre par son nom.
     *
     * @param name Le nom du paramètre
     * @return LE RTAReportParameter associé au nom reçu en paramètre.
     */
    public static RTAReportParameter getByPropertyName(String name) {
        for (RTAReportParameter parameter : values()) {
            if (parameter.getPropertyName().equals(name)) {
                return parameter;
            }
        }
        return null;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public boolean isWellKnown() {
        return wellKnown;
    }

    public boolean isPrintOnReport() {
        return printOnReport;
    }

    public Class getClazz() {
        return clazz;
    }
}
