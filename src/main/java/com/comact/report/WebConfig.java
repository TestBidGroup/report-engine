package com.comact.report;

import com.comact.rest.argument.resolver.DisplayContextResolver;
import com.comact.rest.argument.resolver.FilterArgumentResoler;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

/**
 * User: dgauthier
 * Date: 2016-06-27
 */
@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new DisplayContextResolver());
        argumentResolvers.add(new FilterArgumentResoler());
    }


}
