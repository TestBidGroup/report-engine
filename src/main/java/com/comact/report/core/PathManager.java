package com.comact.report.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;

/**
 * User: dlauzon
 * Date: 2016-12-06
 * Time: 09:01
 */
@Service
public class PathManager {
    private static final String ENV_COMPONENT_DIRECTORY = "COMACT_COMPONENT_DIRECTORY";
    private static final String ENV_COMPONENT_CONFIG_DIRECTORY = "COMACT_COMPONENT_CONFIG_DIRECTORY";
    private static final String ENV_COMPONENT_DATA_DIRECTORY = "COMACT_COMPONENT_DATA_DIRECTORY";
    private static final String ENV_COMPONENT_LOGS_DIRECTORY = "COMACT_COMPONENT_LOGS_DIRECTORY";
    private static final String COMACT_HOME_ENVIRONMENT_VARIABLE_NAME = "COMACT_HOME";

    private static final String DATA_DIRECTORY_NAME = "Data";
    private static final String CONFIG_PATH = "Config";
    private static final String LOGS_DIRECTORY_NAME = "Logs";

    private static final Logger logger = LoggerFactory.getLogger(PathManager.class);

    private String dataPath;
    private String configPath;
    private String applicationPath;
    private String logsPath;

    @PostConstruct
    public void init(){
        applicationPath = System.getenv(ENV_COMPONENT_DIRECTORY);
        if(System.getenv(ENV_COMPONENT_DIRECTORY) != null) {
            applicationPath = System.getenv(ENV_COMPONENT_DIRECTORY);
        } else {
            applicationPath = getComactHomePath();
        }
        if(System.getenv(ENV_COMPONENT_DATA_DIRECTORY) != null) {
            dataPath = System.getenv(ENV_COMPONENT_DATA_DIRECTORY);
        } else {
            dataPath = applicationPath + File.separator + DATA_DIRECTORY_NAME;
        }
        if(System.getenv(ENV_COMPONENT_CONFIG_DIRECTORY) != null) {
            configPath = System.getenv(ENV_COMPONENT_CONFIG_DIRECTORY);
        } else {
            configPath = applicationPath + File.separator + CONFIG_PATH;
        }
        if(System.getenv(ENV_COMPONENT_LOGS_DIRECTORY) != null) {
            logsPath = System.getenv(ENV_COMPONENT_LOGS_DIRECTORY);
        } else {
            logsPath = applicationPath + File.separator + LOGS_DIRECTORY_NAME;
        }
    }

    public static String getComactHomePath() {
        String comactHome = System.getProperty(COMACT_HOME_ENVIRONMENT_VARIABLE_NAME);

        if (comactHome == null) {
            comactHome = System.getenv(COMACT_HOME_ENVIRONMENT_VARIABLE_NAME);

            if (comactHome == null) {
                comactHome = System.getProperty("user.home");
                logger.info("Comact home path was set from user.home property.");
            } else {
                logger.info("Comact home path was set from system environment variable.");
            }
        } else {
            logger.info("Comact home path was set from VM parameter.");
        }

        return comactHome + File.separator + "report";
    }
    public static String getMainClassName()
    {
        StackTraceElement trace[] = Thread.currentThread().getStackTrace();
        if (trace.length > 0) {
            return trace[trace.length - 1].getClassName();
        }
        return "Unknown";
    }

    public String getDataPath() {
        return dataPath;
    }

    public String getDataFilePath(String filename) {
        return dataPath + File.separator + filename;
    }

    public String getConfigPath() {
        return configPath;
    }

    public String getConfigFilePath(String filename) {
        return configPath + File.separator + filename;
    }

    public String getApplicationPath() {
        return applicationPath;
    }

    public String getLogsPath() {
        return logsPath;
    }

    public String getLogsFilePath(String filename) {
        return logsPath + File.separator + filename;
    }
}
