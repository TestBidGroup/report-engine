/*
 * ObjectCodecException.java
 *
 * Copyright (c) 2013 Comact Optimisation, Inc. All rights reserverd.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.comact.report.codec;

import com.comact.localization.LocalizedException;
import com.comact.localization.LocalizedString;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-10-21 (14:37:31)
 * @since 1.4
 */
public class ObjectCodecException extends LocalizedException {

    public final static LocalizedString IMPOSSIBLE_TO_ENCODE_OBJECT_MESSAGE = new LocalizedString(new String[]{"Impossible d'encoder l'objet.", "Impossible to encode object.", "Impossível de codificar o objeto."});
    public final static LocalizedString IMPOSSIBLE_TO_DECODE_OBJECT_MESSAGE = new LocalizedString(new String[]{"Impossible de décoder l'objet.", "Impossible to decode object.", "Impossível de descodificar o objeto."});

    public ObjectCodecException(Throwable cause) {
        super(cause);
    }

    public ObjectCodecException(String message) {
        super(message);
    }

    public ObjectCodecException(String message, Throwable cause) {
        super(message, cause);
    }

    public ObjectCodecException(LocalizedString localizedMessage) {
        super(localizedMessage);
    }

    public ObjectCodecException(LocalizedString localizedMessage, Throwable cause) {
        super(localizedMessage, cause);
    }
}
