/*
 * ObjectCodec.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.codec;


import com.comact.report.converter.ObjectConversionContext;
import com.comact.report.converter.ObjectConverter;

/**
 * Type the interface description here.
 *
 * @author slacasse
 * @version 1.0, 2004-10-21 (14:29:57)
 * @since 1.4
 */
public interface ObjectCodec<EO, DO, CC extends ObjectConversionContext>
{

    //public ObjectConverter<DO, EO, CC> getEncodingConverter();

    //public ObjectConverter<EO, DO, CC> getDecodingConverter();


    public void setConverters(ObjectConverter<DO, EO, CC> encodingConverter,
                              ObjectConverter<EO, DO, CC> decodingConverter,
                              CC conversionContext) throws ObjectCodecException;

    public CC getConversionContext();

    public void setConversionContext(CC conversionContext);

    public EO encodeObject(DO decodedObject) throws ObjectCodecException;

    public DO decodeObject(EO encodedObject) throws ObjectCodecException;

    public boolean isAcceptingNullValue();

}
