/*
 * BirtCalendarCodec.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.codec;


import com.comact.report.converter.CalendarToDateConverter;
import com.comact.report.converter.DateToCalendarConverter;
import com.comact.report.converter.ObjectConversionContext;
import com.comact.report.converter.ObjectConverterException;

import java.util.Calendar;
import java.util.Date;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-10-22 (10:24:36)
 * @since 1.4
 */
public class BirtCalendarCodec extends AbstractObjectCodec<Date, Calendar, ObjectConversionContext>
{

    public BirtCalendarCodec()
    {
        super();

        try
        {
            setConverters( new CalendarToDateConverter(),
                           new DateToCalendarConverter(),
                           null );
        }
        catch( ObjectConverterException e )
        {
            logger.error( null, e );
        }
        catch( ObjectCodecException e )
        {
            logger.error( null, e );
        }
    }
}
