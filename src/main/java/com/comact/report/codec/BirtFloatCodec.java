/*
 * BirtFloatCodec.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.codec;

import com.comact.report.converter.DoubleToFloatConverter;
import com.comact.report.converter.FloatToDoubleConverter;
import com.comact.report.converter.ObjectConversionContext;
import com.comact.report.converter.ObjectConverterException;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-10-22 (10:24:36)
 * @since 1.4
 */
public class BirtFloatCodec extends AbstractObjectCodec<Double, Float, ObjectConversionContext>
{

    public BirtFloatCodec()
    {
        super();

        try
        {
            setConverters( new FloatToDoubleConverter(),
                           new DoubleToFloatConverter(),
                           null );
        }
        catch( ObjectConverterException e )
        {
            logger.error( null, e );
        }
        catch( ObjectCodecException e )
        {
            logger.error( null, e );
        }
    }

}
