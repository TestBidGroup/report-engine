/*
 * CalendarExpressionCodec.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.codec;


import com.comact.report.converter.CalendarConversionContext;
import com.comact.report.converter.CalendarToStringConverter;
import com.comact.report.converter.ObjectConverterException;
import com.comact.report.converter.StringToCalendarConverter;

import java.util.Calendar;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-10-21 (13:39:07)
 * @since 1.4
 */
public class CalendarExpressionCodec extends AbstractObjectCodec<String, Calendar, CalendarConversionContext>
{

    public CalendarExpressionCodec()
    {
        this( null );
    }

    public CalendarExpressionCodec(CalendarConversionContext calendarConversionContext )
    {
        super();

        try
        {
            setConverters( new CalendarToStringConverter( calendarConversionContext ),
                           new StringToCalendarConverter( calendarConversionContext ),
                           null );
        }
        catch( ObjectConverterException e )
        {
            logger.error( null, e );
        }
        catch( ObjectCodecException e )
        {
            logger.error( null, e );
        }
    }

}
