/*
 * BirtLongCodec.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.codec;


import com.comact.report.converter.BigIntegerToLongConverter;
import com.comact.report.converter.LongToBigIntegerConverter;
import com.comact.report.converter.ObjectConversionContext;
import com.comact.report.converter.ObjectConverterException;

import java.math.BigInteger;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-10-22 (10:24:36)
 * @since 1.4
 */
public class BirtLongCodec extends AbstractObjectCodec<BigInteger, Long, ObjectConversionContext>
{

    public BirtLongCodec()
    {
        super();

        try
        {
            setConverters( new LongToBigIntegerConverter(),
                           new BigIntegerToLongConverter(),
                           null );
        }
        catch( ObjectConverterException e )
        {
            logger.error( null, e );
        }
        catch( ObjectCodecException e )
        {
            logger.error( null, e );
        }
    }

}
