/*
 * AbstractObjectCodec.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.codec;

import com.comact.report.converter.ObjectConversionContext;
import com.comact.report.converter.ObjectConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-10-21 (14:33:31)
 * @since 1.4
 */
public abstract class AbstractObjectCodec<EO, DO, CC extends ObjectConversionContext> implements ObjectCodec<EO, DO, CC> {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	protected ObjectConverter<DO, EO, CC> encodingConverter;
	protected ObjectConverter<EO, DO, CC> decodingConverter;

	protected CC conversionContext;

	protected AbstractObjectCodec() {
	}

	protected AbstractObjectCodec(ObjectConverter<DO, EO, CC> encodingConverter,
								  ObjectConverter<EO, DO, CC> decodingConverter) throws ObjectCodecException {
		this(encodingConverter, decodingConverter, null);
	}

	protected AbstractObjectCodec(ObjectConverter<DO, EO, CC> encodingConverter,
								  ObjectConverter<EO, DO, CC> decodingConverter,
								  CC conversionContext) throws ObjectCodecException {
		setConverters(encodingConverter, decodingConverter, conversionContext);
	}

	public void setConverters(ObjectConverter<DO, EO, CC> encodingConverter,
							  ObjectConverter<EO, DO, CC> decodingConverter,
							  CC conversionContext) throws ObjectCodecException {
		validateConvertersCompatibility(encodingConverter, decodingConverter);

		this.encodingConverter = encodingConverter;
		this.decodingConverter = decodingConverter;

		setConversionContext(conversionContext);
	}

	public CC getConversionContext() {
		return conversionContext;
	}

	public void setConversionContext(CC conversionContext) {
		if (conversionContext != null) {
			this.conversionContext = conversionContext;

			if (encodingConverter != null) {
				encodingConverter.setObjectConversionContext(conversionContext);
			}

			if (decodingConverter != null) {
				decodingConverter.setObjectConversionContext(conversionContext);
			}
		} else {
			if (encodingConverter != null) {
				this.conversionContext = encodingConverter.getObjectConversionContext();

				if (decodingConverter != null) {
					decodingConverter.setObjectConversionContext(this.conversionContext);
				}
			}
		}
	}

	public EO encodeObject(DO decodedObject) throws ObjectCodecException {
		EO encodedObject = null;

		try {
			if (encodingConverter == null) {
				throw new ObjectCodecException(new NullPointerException("Can't encode with a null encoding converter."));
			}

			encodedObject = encodingConverter.convertObject(decodedObject);
		} catch (Exception e) {
			throw new ObjectCodecException(ObjectCodecException.IMPOSSIBLE_TO_ENCODE_OBJECT_MESSAGE, e);
		}

		return encodedObject;
	}

	public DO decodeObject(EO encodedObject) throws ObjectCodecException {
		DO decodedObject = null;

		try {
			if (decodingConverter == null) {
				throw new ObjectCodecException(new NullPointerException("Can't decode with a null decoding converter."));
			}

			decodedObject = decodingConverter.convertObject(encodedObject);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new ObjectCodecException(ObjectCodecException.IMPOSSIBLE_TO_DECODE_OBJECT_MESSAGE, e);
		}

		return decodedObject;
	}

	protected static void validateConvertersCompatibility(ObjectConverter encodingConverter,
														  ObjectConverter decodingConverter) throws ObjectCodecException {
		if (encodingConverter == null) {
			throw new ObjectCodecException("The encoding converter is not set.", new NullPointerException());
		}

		if (decodingConverter == null) {
			throw new ObjectCodecException("The decoding object converter is not set.", new NullPointerException());
		}

		if (encodingConverter.getSrcObjectClass().equals(decodingConverter.getDstObjectClass()) == false) {
			throw new ObjectCodecException("The encoding converter source object class must be equal to the decoding converter destination object.");
		}

		if (decodingConverter.getSrcObjectClass().equals(encodingConverter.getDstObjectClass()) == false) {
			throw new ObjectCodecException("The decoding converter source object class must be equal to the encoding converter destination object.");
		}
	}

	public boolean isAcceptingNullValue() {
		return false;
	}
}
