package com.comact.report.service;

import com.comact.report.bean.ReportParameterV1;
import com.comact.report.bean.ReportRequestBeanV1;
import com.comact.report.core.PathManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * User: dlauzon
 * Date: 2016-12-14
 * Time: 15:46
 */
@Service
public class BirtReportService {

    public static final String REPORTS_TEMPLATE_FOLDER = "/reports";
    public static final String REPORTS_OVERRIDES_FOLDER = "/overrides";
    public static final String REPORT_EXTENTION = ".rptdesign";
    public static final String CUSTOMER_REPORT_PREFIX = "C_RTA_";
    @Autowired
    PathManager pathManager;
    @Value("custom.report.template")
    private String customReportTemplate;

    @Autowired
    BirtEngineService birtEngineService;
    private Logger LOGGER = LoggerFactory.getLogger(BirtReportService.class.getSimpleName());
    private File base;
    private Map<String, Collection<ReportParameterV1>> reportParameterCache = new HashMap<>();

    @PostConstruct
    public void init(){
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        final String[] customReportNames = customReportTemplate !=null? customReportTemplate.split(","):new String[0];
        base = new File(pathManager.getDataFilePath(REPORTS_TEMPLATE_FOLDER));
        File override = new File(pathManager.getDataFilePath(REPORTS_OVERRIDES_FOLDER));
        resolver.setPathMatcher(new AntPathMatcher() {
            @Override
            public boolean match(String pattern, String path) {

                boolean match = super.match(pattern, path);
                if (match) {
                    // Valide si il font partir des noms des rapports custom du client
                    boolean isCustomReport = false;
                    for (String customReportName : customReportNames) {
                        if (!customReportName.isEmpty()) {
                            isCustomReport = path.contains(customReportName);
                            if (isCustomReport) {
                                break;
                            }
                        }
                    }

                    if (path.contains(CUSTOMER_REPORT_PREFIX) && !isCustomReport) {
                        return false;
                    }
                }
                return match;
            }
        });
        try {
            clearFolder(base);
            Resource[] datas = resolver.getResources("classpath*:/reports/*");
            copyFromResources(datas, base);
            if (!override.exists()) {
                override.mkdir();
            }

            copyFromOverrides(override, base);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Scheduled(fixedDelay = 60000)
    public void update() {
        Map<String, Collection<ReportParameterV1>> newCache = new HashMap<>();
        if (base != null) {
            File[] files = base.listFiles((File pathname) -> {
                String name = pathname.getName();
                return name.endsWith(REPORT_EXTENTION);
            });
            if (files != null) {
                for (File file : files) {
                    newCache.computeIfAbsent(file.getName().replace(REPORT_EXTENTION, ""), birtEngineService::getParametersValue);
                }
            }
        }
        reportParameterCache = newCache;
    }

    /**
     * Copie les fichiers de rapports à partir des ressources contenue dans le war.
     *
     * @param resources Les ressources qui proviennent du .war
     * @param dest      Le répertoire de base des rapports.
     * @throws IOException Si un problème survient lors de la copie des fichiers.
     */
    private void copyFromResources(Resource[] resources, File dest) throws IOException {
        for (Resource resource : resources) {
            String filename = resource.getFilename();
            try (InputStream in = resource.getURL().openStream();
                 OutputStream out = new FileOutputStream(new File(dest, filename))) {

                byte[] buffer = new byte[1024];

                int length;
                //copy the file content in bytes
                while ((length = in.read(buffer)) > 0) {
                    out.write(buffer, 0, length);
                }
            }
        }
    }

    /**
     * Permet de copier les fichier d'overrides vers le répertoire des rapports de base.
     *
     * @param src  Le Répertoire des overrides.
     * @param dest le répertoire de base des rapports.
     * @throws IOException Si un problème survient lors de la copie des fichiers.
     */
    private void copyFromOverrides(File src, File dest) throws IOException {
        if (src.isDirectory()) {

            //if directory not exists, create it
            if (!dest.exists()) {
                dest.mkdir();
            }

            //list all the directory contents
            String files[] = src.list();

            for (String file : files) {
                //construct the src and dest file structure
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                //recursive copy
                copyFromOverrides(srcFile, destFile);
            }

        } else {
            //if file, then copy it
            //Use bytes stream to support all file types
            try (InputStream in = new FileInputStream(src);
                 OutputStream out = new FileOutputStream(dest)) {

                byte[] buffer = new byte[1024];

                int length;
                //copy the file content in bytes
                while ((length = in.read(buffer)) > 0) {
                    out.write(buffer, 0, length);
                }
            }
        }
    }

    /**
     * Permet de vider un dossier
     *
     * @param base Le dosssier qu'on veut vider.
     * @throws SecurityException Survient lors de la suppression d'un fichier.
     */
    private void clearFolder(File base) throws IOException {
        File[] files = base.listFiles();
        if (files != null) {
            for (File file : files) {
                try {
                    if (!file.delete()) {
                        LOGGER.warn("Impossible to delete the file " + file.getName());
                    }
                } catch (SecurityException ex) {
                    LOGGER.warn("Impossible to delete the file " + file.getName(), ex);
                }
            }
        }
    }

    public String[] getReportList() {
        Set<String> reportNames = reportParameterCache.keySet();
        return reportNames.toArray(new String[reportNames.size()]);
    }

    public Collection<ReportParameterV1> getReportParameters(String template) {
        return reportParameterCache.get(template);
    }

    public long submit(ReportRequestBeanV1 requestBean) {
        return birtEngineService.submit(requestBean);
    }

    public boolean isDone(long token) {
        return birtEngineService.isDone(token);
    }
    public void cancel(long token) {
        birtEngineService.cancel(token);
    }
    public byte[] get(long token) throws Exception {
        return birtEngineService.get(token);
    }
}