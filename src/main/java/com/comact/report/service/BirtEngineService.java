package com.comact.report.service;

import com.comact.BirtTreeCascadingParameter;
import com.comact.CurrencySymbol;
import com.comact.concurrency.task.*;
import com.comact.lang.cache.LimitedCache;
import com.comact.language.Language;
import com.comact.localization.bean.LocalizedStringBean;
import com.comact.report.PairWrapper;
import com.comact.report.bean.ReportParameterV1;
import com.comact.report.bean.ReportRequestBeanV1;
import com.comact.report.bean.value.RTACascadingReportParameterValueBean;
import com.comact.report.bean.value.RTAReportParameterValue;
import com.comact.report.bean.value.RTAReportParameterValueBean;
import com.comact.report.codec.*;
import com.comact.report.core.PathManager;
import com.comact.util.BirtUtil;
import org.slf4j.Logger;
import org.eclipse.birt.report.engine.api.*;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Files;
import java.util.*;


/**
 * User: dlauzon
 * Date: 2017-11-13
 * Time: 14:55
 */
@Service
public class BirtEngineService {
    private static final String REPORT_FILE_PREFIX = "report_";
    private static final String GENERATED_DIRECTORY = "generated";

    @Value("${max.saved.reports}")
    private int maxSavedReports;

    @Autowired
    PathManager pathManager;

    private ReportEngine engine;
    private LimitedCache<IReportRunnable> reportCache;
    private HashMap<Object, Class> birtValueClassToValueClass;
    private HashMap<Class, ObjectCodec> valueClassToEncoderMap = new HashMap<>();
    private HashMap<Class, ObjectCodec> valuesClassToDecoderMapper = new HashMap<>();


    private Logger LOGGER = LoggerFactory.getLogger(BirtEngineService.class.getSimpleName());
    private TaskExecutor taskExecutor = TaskExecutor.getTaskExecutor("report-renderer", 4);
    private Map<Long, OngoingTaskRessourceProxy<byte[]>> taskMap = new HashMap<>();
    private long lastToken;

    @PostConstruct
    public void init() {
        EngineConfig config = new EngineConfig();
        reportCache = new LimitedCache<>(50);
        if (engine == null) {
            engine = new org.eclipse.birt.report.engine.api.ReportEngine(config);
        }
        birtValueClassToValueClass = new HashMap<>();
        BirtCalendarCodec birtCalendarCodec = new BirtCalendarCodec();
        BirtLongCodec birtLongCodec = new BirtLongCodec();
        BirtFloatCodec birtFloatCodec = new BirtFloatCodec();
        BirtDoubleCodec birtDoubleCodec = new BirtDoubleCodec();
        mapValueClassCodec(Date.class, Calendar.class, birtCalendarCodec);
        mapValueClassCodec(Date.class, GregorianCalendar.class, birtCalendarCodec);
        mapValueClassCodec(BigInteger.class, Long.class, birtLongCodec);
        mapValueClassCodec(Double.class, Float.class, birtFloatCodec);
        mapValueClassCodec(BigDecimal.class, Double.class, birtDoubleCodec);
        birtValueClassToValueClass.put(IScalarParameterDefn.TYPE_ANY, Object.class);
        birtValueClassToValueClass.put(IScalarParameterDefn.TYPE_BOOLEAN, Boolean.class);
        birtValueClassToValueClass.put(IScalarParameterDefn.TYPE_DATE_TIME, Calendar.class);
        birtValueClassToValueClass.put(IScalarParameterDefn.TYPE_INTEGER, Integer.class);
        birtValueClassToValueClass.put(IScalarParameterDefn.TYPE_DECIMAL, Double.class);
        birtValueClassToValueClass.put(IScalarParameterDefn.TYPE_FLOAT, Double.class);
        birtValueClassToValueClass.put(IScalarParameterDefn.TYPE_STRING, String.class);

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

        lastToken = getLastToken(true);
    }

    @PreDestroy
    public void dispose() {
        TaskExecutor.release(taskExecutor);
    }


    protected void mapValueClassCodec(Class encodedClass, Class decodedClass, ObjectCodec coDec) {
        valueClassToEncoderMap.put(decodedClass, coDec);
        valuesClassToDecoderMapper.put(encodedClass, coDec);
    }

    /**
     * Permet de retourner une liste de Task préinitialisée en fonction des locales supporté par le système.
     *
     * @param report Le rapport pour lequel on veut initialisé les tasks
     * @return Un map contenant les tasks en fonction des langues supportés dans le système.
     */
    private Map<Language, IGetParameterDefinitionTask> getTasks(String report) {
        Map<Language, IGetParameterDefinitionTask> tasksMap = new HashMap<>();
        IReportRunnable design = getReport(report);
        for (Language language : Language.values()) {
            IGetParameterDefinitionTask task = engine.createGetParameterDefinitionTask(design);
            // Only to generate text
            Locale locale = getLocale(language, CurrencySymbol.DOLLAR);
            task.setLocale(locale);
            tasksMap.put(language, task);
        }

        return tasksMap;
    }

    public static Locale getLocale(Language language, CurrencySymbol symbol) {
        Locale locale = language.getLocale();
        if (symbol.currencyName().equals(CurrencySymbol.DOLLAR.currencyName())) {
            if (locale.getLanguage().equals("pt")) {
                locale = new Locale(locale.getLanguage(), "CA");
            }
        } else if (symbol.currencyName().equals(CurrencySymbol.EURO.currencyName())) {
            if (locale.getLanguage().equals("fr")) {
                locale = Locale.FRANCE;
            } else if (locale.getLanguage().equals("en")) {
                locale = new Locale("en", "IE");
            } else {
                locale = new Locale(locale.getLanguage(), "PT");
            }
        } else if (symbol.currencyName().equals(CurrencySymbol.REAL.currencyName())) {
            locale = new Locale(locale.getLanguage(), "br");
        }
        return locale;
    }

    /**
     * Retourne un map des valeurs possibles pour les paramètres.
     *
     * @param report Le rapport à analyser.
     * @return Le Map qui contient les valeurs.
     */
    public Collection<ReportParameterV1> getParametersValue(String report) {
        Map<String, ReportParameterV1> ret = null;
        Map<Language, IGetParameterDefinitionTask> tasks = null;
        try {
            tasks = getTasks(report);
            ret = getParametersValue(tasks);
        } finally {
            if (tasks != null) {
                for (IGetParameterDefinitionTask task : tasks.values()) {
                    if (task != null) {
                        task.close();
                    }
                }
            }
        }

        return ret.values();
    }

    /**
     * Retourne un map des valeurs possibles pour les paramètres.
     *
     * @param tasks les taches préparées
     * @return Le map qui contient les valeurs.
     * @throws Exception
     */
    private Map<String, ReportParameterV1> getParametersValue(Map<Language, IGetParameterDefinitionTask> tasks) {
        Map<String, ReportParameterV1> ret = new HashMap<>();

        boolean first = true;
        for (Language language : tasks.keySet()) {
            IGetParameterDefinitionTask task = tasks.get(language);

            // passe true pour avoir les éléments avec leur group. Ainsi on peut gérer les paramètres cascades.
            Collection<PairWrapper<IScalarParameterDefn, IParameterGroupDefn>> wrapedParams = wrapParameters(task.getParameterDefns(true));
            for (PairWrapper<IScalarParameterDefn, IParameterGroupDefn> pair : wrapedParams) {
                if (pair != null) {
                    IScalarParameterDefn parameterDefn = pair.getFirst();
                    IParameterGroupDefn group = pair.getSecond();

                    if (group instanceof ICascadingParameterGroup) {
                        BirtTreeCascadingParameter treeCascadingParameter = getTreeCascadingParameter(pair, task);

                        // Itère dans tous les éléments que contient le groupe de paramètre cascade.
                        ArrayList<IScalarParameterDefn> children = group.getContents();
                        for (IScalarParameterDefn child : children) {
                            if (first) {
                                ret.put(child.getName(), getAvailableValues(child, treeCascadingParameter, language));
                            } else {
                                updateLabelAvailableValues(ret.get(child.getName()), child, treeCascadingParameter, language);
                            }
                        }
                    } else {
                        if (first) {
                            ret.put(parameterDefn.getName(), getAvailableValues(parameterDefn, task.getSelectionList(parameterDefn.getName()), language));
                        } else {
                            updateLabelAvailableValues(ret.get(parameterDefn.getName()), parameterDefn, task.getSelectionList(parameterDefn.getName()), language);
                        }
                    }
                }
            }
            first = false;
        }

        return ret;
    }

    /**
     * Construit un arbre qui contient toutes les possibilités pour les cascading parameters.
     *
     * @param pair Le couple paramètre parent et son groupe.
     * @param task La tache Birt fraichement initialisé.
     * @return retourne l'arbre construit.
     */
    private BirtTreeCascadingParameter getTreeCascadingParameter(PairWrapper<IScalarParameterDefn, IParameterGroupDefn> pair, IGetParameterDefinitionTask task) {
        Collection<IParameterSelectionChoice> selectionChoices = task.getSelectionTreeForCascadingGroup(pair.getSecond().getName());

        // Construction de l'arbre des choix de sélections.
        BirtTreeCascadingParameter treeCascadingParameter = new BirtTreeCascadingParameter();
        for (IParameterSelectionChoice parameterSelectionChoice : selectionChoices) {
            fillNode(parameterSelectionChoice, treeCascadingParameter.getRoot(), pair.getFirst(), (ICascadingParameterGroup) pair.getSecond());
        }

        return treeCascadingParameter;
    }

    /**
     * Méthode récursive permettant de créer l'arbre des valeurs pour un CascadingParameters.
     *
     * @param parameterSelectionChoice la valeur du paramètre qui va être traité
     * @param nodeParent               le noeud parent à ce paramètre.
     * @param param                    Le paramètre qui va être traité
     * @param group                    Le group auquel le paramètre appartient.
     */
    private void fillNode(IParameterSelectionChoice parameterSelectionChoice, BirtTreeCascadingParameter.BirtNodeCascadingParameter nodeParent, IScalarParameterDefn param, ICascadingParameterGroup group) {
        BirtTreeCascadingParameter.BirtNodeCascadingParameter node = new BirtTreeCascadingParameter.BirtNodeCascadingParameter();
        Object decodedObject = decode(parameterSelectionChoice.getValue());
        node.setValue(decodedObject);

        node.setTagName(parameterSelectionChoice.getLabel());
        node.setParent(nodeParent);
        node.setParamName(param.getName());
        node.setChildren(new ArrayList<>());
        nodeParent.getChildren().add(node);

        if (parameterSelectionChoice instanceof ICascadingParameterSelectionChoice) {
            ICascadingParameterSelectionChoice cascadingParameterSelectionChoice = (ICascadingParameterSelectionChoice) parameterSelectionChoice;
            Collection<ICascadingParameterSelectionChoice> selectionChoices = cascadingParameterSelectionChoice.getChildSelectionList();
            if (selectionChoices != null) {
                // Retrouve le prochain paramètre dans la liste.
                IScalarParameterDefn nextParam = getNextParam(group, param.getName());
                for (ICascadingParameterSelectionChoice selectionChoice : selectionChoices) {
                    fillNode(selectionChoice, node, nextParam, group);
                }
            }
        }
    }

    protected Object decode(Object objectToDecode) throws ObjectCodecException {
        Object retValue = objectToDecode;
        if (objectToDecode != null) {
            ObjectCodec codec = valuesClassToDecoderMapper.get(objectToDecode.getClass());

            if (codec != null) {
                retValue = codec.decodeObject(objectToDecode);
            }
        }
        return retValue;
    }

    /**
     * Méthode permettant de retrouver le prochain paramètre pour les paramètres cascades.
     *
     * @param group            Le groupe dans lequel se trouve les paramètres cascades
     * @param nameCurrentParam Le nom du paramètre actuel.
     * @return Le prochain paramètre en fonction du paramètre courant.
     */
    private IScalarParameterDefn getNextParam(ICascadingParameterGroup group, String nameCurrentParam) {
        ArrayList<IScalarParameterDefn> paramList = group.getContents();
        IScalarParameterDefn nextParam = null;

        for (int i = 0; i < paramList.size() && nextParam == null; i++) {
            IScalarParameterDefn temp = paramList.get(i);
            if (temp.getName().equals(nameCurrentParam) && i != (paramList.size() - 1)) {
                nextParam = paramList.get(i + 1);
            }
        }

        return nextParam;
    }

    private void updateDisplayName(ReportParameterV1 parameterV1, IScalarParameterDefn param, Language language) {
        if (parameterV1.getLabel() == null) {
            parameterV1.setLabel(new LocalizedStringBean());
        }
        String displayName = param.getPromptText();
        if (displayName == null || displayName.isEmpty()) {
            displayName = param.getName();
        }
        parameterV1.getLabel().set(language, displayName);
    }

    /**
     * Retrouve la liste des labels/ids pour le paramètre
     *
     * @param param                  le paramètre
     * @param treeCascadingParameter L'arbre qui contient les valeurs des paramètres en cascade.
     * @param language
     * @return Une collection de Valeur de paramètre.
     * @throws Exception
     */
    private ReportParameterV1 getAvailableValues(IScalarParameterDefn param, BirtTreeCascadingParameter treeCascadingParameter, Language language) {
        ReportParameterV1 parameterV1 = new ReportParameterV1();

        updateDisplayName(parameterV1, param, language);
        Collection<RTAReportParameterValue> reportParameterValues = new ArrayList<>();
        String parent = null;
        Collection<BirtTreeCascadingParameter.BirtNodeCascadingParameter> nodes = treeCascadingParameter.getNodesByParamName(param.getName());
        for (BirtTreeCascadingParameter.BirtNodeCascadingParameter node : nodes) {
            RTACascadingReportParameterValueBean cascadingReportParameterValue = new RTACascadingReportParameterValueBean();
            parent = node.getParent().getParamName();
            cascadingReportParameterValue.setParentName(node.getParent().getParamName());
            cascadingReportParameterValue.setParentValue(node.getParent().getValue());
            cascadingReportParameterValue.setValue(node.getValue());
            LocalizedStringBean label = new LocalizedStringBean();
            label.set(language, node.getTagName());
            cascadingReportParameterValue.setLabel(label);
            reportParameterValues.add(cascadingReportParameterValue);
            parameterV1.setDefaultValue(cascadingReportParameterValue);
        }
        Class clazz = birtValueClassToValueClass.get(param.getDataType());
        parameterV1.setParent(parent);
        parameterV1.setCascading(true);

        parameterV1.setName(param.getName());
        parameterV1.setType(clazz.getSimpleName());
        parameterV1.setChoices(reportParameterValues.toArray(new RTAReportParameterValue[reportParameterValues.size()]));
        return parameterV1;
    }

    /**
     * Retrouve la liste des labels/ids pour le paramètre
     *
     * @param param                     le paramètre
     * @param parameterSelectionChoices les choix possible pour ce paramètre.
     * @param language
     * @return Une collection de Valeur de paramètre.
     * @throws Exception
     */
    private ReportParameterV1 getAvailableValues(IScalarParameterDefn param, Collection<IParameterSelectionChoice> parameterSelectionChoices, Language language) {
        ReportParameterV1 parameterV1 = new ReportParameterV1();
        updateDisplayName(parameterV1, param, language);
        Collection<RTAReportParameterValue> choices = new Vector<>();
        Class clazz = birtValueClassToValueClass.get(param.getDataType());
        for (IParameterSelectionChoice selectionItem : parameterSelectionChoices) {
            RTAReportParameterValueBean rtaReportParameterValueBean = BirtUtil.createDefaultValue(clazz, selectionItem.getValue().toString());
            String label = selectionItem.getLabel();
            LocalizedStringBean localizedLabel = new LocalizedStringBean();
            localizedLabel.set(language, label);

            rtaReportParameterValueBean.setLabel(localizedLabel);
            choices.add(rtaReportParameterValueBean);
            parameterV1.setDefaultValue(rtaReportParameterValueBean);
        }
        parameterV1.setName(param.getName());
        if (param.getControlType() == IScalarParameterDefn.LIST_BOX) {
            if (param.getScalarParameterType().equals("multi-value")) {
                parameterV1.setMultiSelect(true);
            }

            parameterV1.setChoices(choices.toArray(new RTAReportParameterValue[choices.size()]));
        } else {
            parameterV1.setDefaultValue(BirtUtil.createDefaultValue(clazz, param.getDefaultValue()));
        }
        parameterV1.setType(clazz.getSimpleName());
        return parameterV1;
    }

    /**
     * Fait une mise à jour des displayText pour une langue donnée
     * Pour les paramètres cascades.
     *
     * @param reportParameterV1      La liste des paramètres.
     * @param param                  le paramètre
     * @param treeCascadingParameter Le tree qui repérsente les choix de paramètre
     * @param language               la langue dans laquelle on veut les choix
     */
    private void updateLabelAvailableValues(ReportParameterV1 reportParameterV1, IScalarParameterDefn param, BirtTreeCascadingParameter treeCascadingParameter, Language language) {
        updateDisplayName(reportParameterV1, param, language);
        Collection<BirtTreeCascadingParameter.BirtNodeCascadingParameter> nodes = treeCascadingParameter.getNodesByParamName(param.getName());
        for (BirtTreeCascadingParameter.BirtNodeCascadingParameter node : nodes) {
            BirtTreeCascadingParameter.BirtNodeCascadingParameter parent = node.getParent();
            for (RTAReportParameterValue parameterValue : reportParameterV1.getChoices()) {
                RTACascadingReportParameterValueBean cascadingReportParameterValueBean = (RTACascadingReportParameterValueBean) parameterValue;
                if (Objects.equals(cascadingReportParameterValueBean.getParentName(), parent.getParamName())
                        && cascadingReportParameterValueBean.getParentValue() == parent.getValue()
                        && cascadingReportParameterValueBean.getValue().equals(node.getValue())) {
                    cascadingReportParameterValueBean.getLabel().set(language, node.getTagName());
                    break;
                }
            }
        }
    }

    /**
     * Fait une mise à jour des displayText pour une langue donnée
     *
     * @param reportParameterV1         La liste des paramètres.
     * @param param                     le paramètre
     * @param parameterSelectionChoices La liste des choix pour le paramètre.
     * @param language                  la langue dans laquelle on veut les choix
     */
    private void updateLabelAvailableValues(ReportParameterV1 reportParameterV1, IScalarParameterDefn param, Collection<IParameterSelectionChoice> parameterSelectionChoices, Language language) {
        updateDisplayName(reportParameterV1, param, language);
        for (IParameterSelectionChoice selectionItem : parameterSelectionChoices) {
            RTAReportParameterValue[] choices = reportParameterV1.getChoices();
            if (choices != null) {
                for (RTAReportParameterValue parameterValue : choices) {
                    boolean equals;
                    Class aClass = birtValueClassToValueClass.get(param.getDataType());

                    if (Double.class.getSimpleName().equals(aClass.getSimpleName())) {
                        Object valueNormalized = BirtUtil.initValueFromBirt(aClass, selectionItem.getValue().toString());
                        equals = Math.abs(((Double) parameterValue.getValue()) - ((Double) valueNormalized)) < 0.1;
                    } else {
                        equals = parameterValue.getValue().equals(selectionItem.getValue());
                    }
                    if (equals) {
                        parameterValue.getLabel().set(language, selectionItem.getLabel());
                        break;
                    }
                }
            }
        }
    }

    /**
     * Wrap chacun des paramètres de Birt dans une combinaison nom du paramètre et groupe.
     *
     * @param params La collection des paramètres qui proviennent de Birt.
     * @return Une collection de PairWrapper.
     * @throws Exception
     */
    private Collection<PairWrapper<IScalarParameterDefn, IParameterGroupDefn>> wrapParameters(Collection params) {
        Iterator iter = params.iterator();
        // Iterate over all parameters
        Vector<PairWrapper<IScalarParameterDefn, IParameterGroupDefn>> paramsExtracted = new Vector<>();
        while (iter.hasNext()) {
            PairWrapper<IScalarParameterDefn, IParameterGroupDefn> pair = new PairWrapper<>();
            IParameterDefnBase param = (IParameterDefnBase) iter.next();
            // Group section found
            if (param instanceof IParameterGroupDefn) {
                // Get Group Name
                IParameterGroupDefn group = (IParameterGroupDefn) param;
                // Get the parameters within a group
                ArrayList contents = group.getContents();
                if (contents.size() > 0) {
                    IScalarParameterDefn scalar = (IScalarParameterDefn) contents.get(0);
                    pair.setFirst(scalar);
                    pair.setSecond(group);
                }
            } else {
                // Parameters are not in a group
                IScalarParameterDefn scalar = (IScalarParameterDefn) param;
                pair.setFirst(scalar);
                pair.setSecond(null);
            }
            paramsExtracted.add(pair);
        }
        return paramsExtracted;
    }


    protected void encodeParameters(Map<String, Object> reportParameters) throws ObjectCodecException {
        if (reportParameters != null) {
            Set<String> parameterNames = reportParameters.keySet();
            for (String parameterName : parameterNames) {
                Object value = reportParameters.get(parameterName);
                Object decodedValue = encode(value);
                reportParameters.put(parameterName, decodedValue);
            }
        }
    }

    protected Object encode(Object objectToEncode) throws ObjectCodecException {
        Object retValue = objectToEncode;
        if (objectToEncode != null) {
            if (objectToEncode instanceof Collection) {
                Object[] array = ((Collection) objectToEncode).toArray();
                for (int i = 0; i < array.length; i++) {
                    ObjectCodec codec = valueClassToEncoderMap.get(array[i].getClass());
                    if (codec != null) {
                        array[i] = codec.encodeObject(array[i]);
                    }
                }
                retValue = array;
            } else {
                ObjectCodec codec = valueClassToEncoderMap.get(objectToEncode.getClass());

                if (codec != null) {
                    retValue = codec.encodeObject(objectToEncode);
                }
            }
        }
        return retValue;
    }

    private synchronized IReportRunnable getReport(String reportName) {
        IReportRunnable reportRunnable = reportCache.get(reportName);
        if (reportRunnable == null) {
            try {
                File file = new File(pathManager.getDataFilePath(BirtReportService.REPORTS_TEMPLATE_FOLDER + "/" +
                        reportName + BirtReportService.REPORT_EXTENTION));
                reportRunnable = engine.openReportDesign(file.getAbsolutePath());
                reportCache.add(reportName, reportRunnable);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        return reportRunnable;

    }

    private Map<String, Object> extractParameters(ReportRequestBeanV1 requestBean) {
        Map<String, Object> params = requestBean.getParams();
        if (params == null) {
            params = new HashMap<>();
        }

        params.put("title", requestBean.getTitle());
        params.put("description", requestBean.getDescription());
        params.put("machine-name", requestBean.getMachineName());

        return params;
    }


    public synchronized long submit(ReportRequestBeanV1 requestBean) {
        long token = ++lastToken;
        OngoingTaskRessourceProxy<byte[]> task = taskExecutor.submit(new ReportRenderingTask(requestBean, token));
        taskMap.put(token, task);
        return token;
    }

    public synchronized boolean isDone(long token) {
        OngoingTaskRessourceProxy<byte[]> task = taskMap.get(token);
        if (task != null)
            return task.isDone();
        else {
            File file = getReportFile(token);
            return file.exists();
        }
    }

    public synchronized void cancel(long token) {
        OngoingTaskRessourceProxy<byte[]> task = taskMap.remove(token);
        if (task != null) {
            task.softCancel();
        }
    }

    public synchronized byte[] get(long token) throws Exception {
        OngoingTaskRessourceProxy<byte[]> task = taskMap.remove(token);
        if (task != null)
            return task.get();
        else {
            File file = getReportFile(token);
            return Files.readAllBytes(file.toPath());
        }
    }

    private File getReportFile(long token) {
        return new File(pathManager.getDataPath() + File.separator + GENERATED_DIRECTORY, REPORT_FILE_PREFIX + String.valueOf(token));
    }

    private synchronized void saveReport(long token, byte[] data) throws IOException {
        if(data != null) {
            File reportDir = new File(pathManager.getDataPath(), GENERATED_DIRECTORY);
            if (!reportDir.exists() && !reportDir.mkdirs())
                throw new IOException("Cannot create directory " + reportDir.getAbsolutePath());

            File file = new File(reportDir, REPORT_FILE_PREFIX + String.valueOf(token));
            Files.write(file.toPath(), data);

            long oldestToken = getLastToken(false);
            if (lastToken - oldestToken > maxSavedReports) {
                File oldest = new File(reportDir, REPORT_FILE_PREFIX + String.valueOf(oldestToken));
                if (oldest.exists() && !oldest.delete())
                    LOGGER.error("Cannot delete oldest report " + oldestToken);
            }
        }
    }

    private long getLastToken(boolean max) {
        long ret = max ? -1 : Long.MAX_VALUE;
        File reportDir = new File(pathManager.getDataPath(), GENERATED_DIRECTORY);
        if (reportDir.exists()) {
            File[] reports = reportDir.listFiles((dir, name) -> name.startsWith(REPORT_FILE_PREFIX));
            if (reports != null) {
                for (File report : reports) {
                    String name = report.getName();
                    String str = name.substring(REPORT_FILE_PREFIX.length());
                    long token = Long.parseLong(str);

                    if (max)
                        ret = Math.max(ret, token);
                    else
                        ret = Math.min(ret, token);
                }
            }
        }

        return ret;
    }

    private class ReportRenderingTask implements OngoingTask<byte[], TaskContext> {
        boolean cancel = false;
        private ReportRequestBeanV1 requestBean;
        private IRunAndRenderTask task;
        private long token;

        private ReportRenderingTask(ReportRequestBeanV1 requestBean, long token) {
            this.requestBean = requestBean;
            this.token = token;
        }

        @Override
        public String getStatus() {
            return null;
        }

        @Override
        public void cancel() {
            cancel = true;
        }

        @Override
        public byte[] call(TaskContext context) throws Exception {
            IReportRunnable report = getReport(requestBean.getTemplate());
            Map<String, Object> parameters = extractParameters(requestBean);
            encodeParameters(parameters);
            ByteArrayOutputStream data = new ByteArrayOutputStream();
            HTMLRenderOption options = new HTMLRenderOption();
            options.setOutputStream(data);
            options.setOutputFormat(requestBean.getFormat());


            task = engine.createRunAndRenderTask(report);

            task.setRenderOption(options);

            task.setLocale(requestBean.getLocale());
            task.setParameterValues(parameters);
            try {
                task.getRenderOption().setOutputStream(data);

                if (!cancel) {
                    task.run();
                    task.close();
                }
                byte[] bytes = data.toByteArray();
                saveReport(token, bytes);
                return bytes;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
            return null;
        }
    }
}
