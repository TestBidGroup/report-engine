/*
 * AbstractObjectConverter.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;


import com.comact.report.validators.ObjectValidator;
import com.comact.report.validators.ObjectValidatorException;

import java.util.Collection;
import java.util.Vector;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-02 (13:03:50)
 * @since 1.4
 */
public abstract class AbstractObjectConverter <SO, DO, CC extends ObjectConversionContext> implements ObjectConverter<SO, DO, CC>
{

    protected final Class<SO> srcObjectClass;
    protected final Class<DO> dstObjectClass;

    protected final Vector<ObjectValidator> srcObjectValidators;

    protected CC objectConversionContext;

    protected AbstractObjectConverter( Class<SO> srcObjectClass, Class<DO> dstObjectClass, CC objectConversionContext ) throws ObjectConverterException
    {
        if( srcObjectClass == null )
        {
            throw new ObjectConverterException( "The source object class of the converter must be non-null.", new NullPointerException() );
        }

        if( dstObjectClass == null )
        {
            throw new ObjectConverterException( "The destination object class of the converter must be non-null.", new NullPointerException() );
        }

        this.srcObjectClass = srcObjectClass;
        this.dstObjectClass = dstObjectClass;

        srcObjectValidators = new Vector<ObjectValidator>();

        setObjectConversionContext( objectConversionContext );
    }

    public final Class<SO> getSrcObjectClass()
    {
        return srcObjectClass;
    }

    public final Class<DO> getDstObjectClass()
    {
        return dstObjectClass;
    }

    public final CC getObjectConversionContext()
    {
        return objectConversionContext;
    }

    public void setObjectConversionContext( CC objectConversionContext )
    {
        this.objectConversionContext = objectConversionContext;
    }

    public final void addSrcObjectValidator( ObjectValidator srcObjectValidator )
    {
        if( srcObjectValidator != null )
        {
            srcObjectValidators.add( srcObjectValidator );
        }
    }

/*    public void removeSrcObjectValidator( ObjectValidator srcObjectValidator )
    {
        if( srcObjectValidator != null )
        {
            srcObjectValidators.remove( srcObjectValidator );
        }
    } */

    public final Collection<ObjectValidator> getSrcObjectValidators()
    {
        return ( Collection<ObjectValidator> ) this.srcObjectValidators.clone();
    }

    protected final void validateSrcObject( SO srcObject ) throws ObjectConverterException
    {
        if( srcObjectValidators != null )
        {
            try
            {

                for( ObjectValidator objectValidator : srcObjectValidators )
                {
                    objectValidator.validateObject( srcObject );
                }
            }
            catch( ObjectValidatorException e )
            {
                throw new ObjectConverterException( ObjectConverterException.INVALID_SOURCE_OBJECT_MSG, e );
            }
        }
    }
}

