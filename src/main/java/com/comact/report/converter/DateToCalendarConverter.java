/*
 * DateToCalendarConverter.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;

import java.util.Calendar;
import java.util.Date;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-04 (10:52:25)
 * @since 1.4
 */
public class DateToCalendarConverter extends AbstractObjectConverter<Date, Calendar, ObjectConversionContext>
{

    public DateToCalendarConverter() throws ObjectConverterException
    {
        super( Date.class, Calendar.class, null );
    }

    public Calendar convertObject( Date aDate ) throws ObjectConverterException
    {
        Calendar aCalendar = null;

        if( aDate != null )
        {
            aCalendar = Calendar.getInstance();
            aCalendar.setTime( aDate );
        }

        return aCalendar;
    }
}
