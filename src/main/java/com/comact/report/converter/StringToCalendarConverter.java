/*
 * StringToCalendarConverter.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;


import com.comact.report.validators.NonNullStringValidator;
import com.comact.report.validators.ObjectValidatorException;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-03 (08:00:18)
 * @since 1.4
 */
public class StringToCalendarConverter extends AbstractObjectConverter<String, Calendar, CalendarConversionContext>
{

    public StringToCalendarConverter() throws ObjectConverterException
    {
        this( null );
    }

    public StringToCalendarConverter( CalendarConversionContext calendarConversionContext ) throws ObjectConverterException
    {
        super( String.class, Calendar.class, calendarConversionContext );

        try
        {
            addSrcObjectValidator( new NonNullStringValidator() );
        }
        catch( ObjectValidatorException e )
        {
            throw new ObjectConverterException( ObjectConverterException.ERROR_CREATING_VALIDATOR_MSG, e );
        }
    }

    public void setObjectConversionContext( CalendarConversionContext calendarConversionContext )
    {
        if( calendarConversionContext != null )
        {
            super.setObjectConversionContext( calendarConversionContext );
        }
        else
        {
            super.setObjectConversionContext( new CalendarConversionContext() );
        }
    }

    public Calendar convertObject( String formattedDateTimeString ) throws ObjectConverterException
    {
        Calendar calendar = null;

        try
        {
            validateSrcObject( formattedDateTimeString );

            calendar = Calendar.getInstance();

            try
            {
                calendar.setTime( objectConversionContext.getDateFromDefaultFormattedDateTimeString( formattedDateTimeString ) );
            }
            catch( ParseException e )
            {
                try
                {
                    calendar.setTime( objectConversionContext.getDateFromBasicLocalizedFormattedDateTimeString( formattedDateTimeString ) );
                }
                catch( ParseException e1 )
                {
                    try
                    {
                        calendar.setTime( objectConversionContext.getDateFromBasicFormattedDateTimeString( formattedDateTimeString ) );
                    }
                    catch( ParseException e2 )
                    {
                        try
                        {
                            calendar.setTime( objectConversionContext.getDateFromPreciseLocalizedFormattedDateTimeString( formattedDateTimeString ) );
                        }
                        catch( ParseException e3 )
                        {
                            calendar.setTime( objectConversionContext.getDateFromPreciseFormattedDateTimeString( formattedDateTimeString ) );
                        }
                    }
                }
            }
        }
        catch( Exception e )
        {
            throw new ObjectConverterException( ObjectConverterException.IMPOSSIBLE_TO_CONVERT_OBJECT_MSG, e );
        }

        return calendar;
    }
}
