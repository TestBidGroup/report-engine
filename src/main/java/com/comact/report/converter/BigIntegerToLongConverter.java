/*
 * BigIntegerToLongConverter.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;

import java.math.BigInteger;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-04 (10:52:25)
 * @since 1.4
 */
public class BigIntegerToLongConverter extends AbstractObjectConverter<BigInteger, Long, ObjectConversionContext>
{

    public BigIntegerToLongConverter() throws ObjectConverterException
    {
        super( BigInteger.class, Long.class, null );
    }

    public Long convertObject( BigInteger aBigInteger ) throws ObjectConverterException
    {
        Long aLong = null;

        if( aBigInteger != null )
        {
            aLong = aBigInteger.longValue();
        }

        return aLong;
    }
}
