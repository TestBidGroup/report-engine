/*
 * LongToBigIntegerConverter.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;

import java.math.BigInteger;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-04 (10:52:25)
 * @since 1.4
 */
public class LongToBigIntegerConverter extends AbstractObjectConverter<Long, BigInteger, ObjectConversionContext>
{

    public LongToBigIntegerConverter() throws ObjectConverterException
    {
        super( Long.class, BigInteger.class, null );
    }

    public BigInteger convertObject( Long aLong ) throws ObjectConverterException
    {
        BigInteger aBigInteger = null;

        if( aLong != null )
        {
            aBigInteger = BigInteger.valueOf( aLong );
        }

        return aBigInteger;
    }
}
