/*
 * ObjectConverter.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;


import com.comact.report.validators.ObjectValidator;

import java.util.Collection;

/**
 * Type the interface description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-02 (09:42:46)
 * @since 1.4
 */
public interface ObjectConverter <SO, DO, CC extends ObjectConversionContext>
{

    public Class<SO> getSrcObjectClass();

    public Class<DO> getDstObjectClass();

    public DO convertObject(SO object) throws ObjectConverterException;

    public CC getObjectConversionContext();

    public void setObjectConversionContext(CC objectConversionContext);

    public void addSrcObjectValidator(ObjectValidator srcObjectValidator);

    //public void removeSrcObjectValidator( ObjectValidator srcObjectValidator );

    public Collection<ObjectValidator> getSrcObjectValidators();

}
