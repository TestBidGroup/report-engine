/*
 * CalendarToDateConverter.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;

import java.util.Calendar;
import java.util.Date;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-04 (10:52:25)
 * @since 1.4
 */
public class CalendarToDateConverter extends AbstractObjectConverter<Calendar, Date, ObjectConversionContext>
{

    public CalendarToDateConverter() throws ObjectConverterException
    {
        super( Calendar.class, Date.class, null );
    }

    public Date convertObject( Calendar aCalendar ) throws ObjectConverterException
    {
        Date aDate = null;

        if( aCalendar != null )
        {
            aDate = aCalendar.getTime();
        }

        return aDate;
    }
}
