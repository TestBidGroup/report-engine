/*
 * FloatToDoubleConverter.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-04 (10:52:25)
 * @since 1.4
 */
public class FloatToDoubleConverter extends AbstractObjectConverter<Float, Double, ObjectConversionContext>
{

    public FloatToDoubleConverter() throws ObjectConverterException
    {
        super( Float.class, Double.class, null );
    }

    public Double convertObject( Float aFloat ) throws ObjectConverterException
    {
        Double aDouble = null;

        if( aFloat != null )
        {
            aDouble = aFloat.doubleValue();
        }

        return aDouble;
    }
}
