/*
 * ObjectConverterException.java
 *
 * Copyright (c) 2013 Comact Optimisation, Inc. All rights reserverd.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.comact.report.converter;

import com.comact.localization.LocalizedException;
import com.comact.localization.LocalizedString;

/**
 * Type the class description here.
 *
 * @author pymathieu
 * @version 1.0, 2004-03-18 (10:33:38)
 * @since 1.4
 */
public class ObjectConverterException extends LocalizedException {

    public final static LocalizedString IMPOSSIBLE_TO_CONVERT_OBJECT_MSG = new LocalizedString("Impossible de convertir l'objet.", "Impossible to convert object.", "Impossível de convertir o objeto.");
    public final static LocalizedString ERROR_CREATING_VALIDATOR_MSG = new LocalizedString("Erreur lors de la création du validateur pour le convertisseur.", "Error creating validator for converter.", "Erro ao criar Validador para o conversor.");
    public final static LocalizedString INVALID_SOURCE_OBJECT_MSG = new LocalizedString("L'objet source du convertisseur est invalide.", "Invalid converter source object.", "Objeto recurso do conversor é inválido.");

    public ObjectConverterException(Throwable cause) {
        super(cause);
    }

    public ObjectConverterException(String message) {
        super(message);
    }

    public ObjectConverterException(String message, Throwable cause) {
        super(message, cause);
    }

    public ObjectConverterException(LocalizedString localizedMessage) {
        super(localizedMessage);
    }

    public ObjectConverterException(LocalizedString localizedMessage, Throwable cause) {
        super(localizedMessage, cause);
    }
}
