/*
 * NumberBitArrayConversionContext.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-10-25 (16:49:36)
 * @since 1.4
 */
public class NumberBitArrayConversionContext implements ObjectConversionContext
{

    public static final int NUMBER_OF_BITS_FROM_NUMBER = Integer.MAX_VALUE;

    private boolean removeTrailingZeros;

    private int maximumNumberOfBits = NUMBER_OF_BITS_FROM_NUMBER;

    public NumberBitArrayConversionContext()
    {
        this( false );
    }

    public NumberBitArrayConversionContext( boolean removeTrailingZeros )
    {
        this( NUMBER_OF_BITS_FROM_NUMBER, removeTrailingZeros );
    }

    public NumberBitArrayConversionContext( int maximumNumberOfBits )
    {
        this( maximumNumberOfBits, false );
    }

    public NumberBitArrayConversionContext( int maximumNumberOfBits, boolean removeTrailingZeros )
    {
        setMaximumNumberOfBits( maximumNumberOfBits );
        setRemoveTrailingZeros( removeTrailingZeros );
    }

    public boolean isRemoveTrailingZeros()
    {
        return removeTrailingZeros;
    }

    public void setRemoveTrailingZeros( boolean removeTrailingZeros )
    {
        this.removeTrailingZeros = removeTrailingZeros;
    }

    public int getMaximumNumberOfBits()
    {
        return maximumNumberOfBits;
    }

    public void setMaximumNumberOfBits( int maximumNumberOfBits )
    {
        this.maximumNumberOfBits = maximumNumberOfBits;
    }
}
