/*
 * BigDecimalToDoubleConverter.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;

import java.math.BigDecimal;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-04 (10:52:25)
 * @since 1.4
 */
public class BigDecimalToDoubleConverter extends AbstractObjectConverter<BigDecimal, Double, ObjectConversionContext>
{

    public BigDecimalToDoubleConverter() throws ObjectConverterException
    {
        super( BigDecimal.class, Double.class, null );
    }

    public Double convertObject( BigDecimal aBigDecimal ) throws ObjectConverterException
    {
        Double aDouble = null;

        if( aBigDecimal != null )
        {
            aDouble = aBigDecimal.doubleValue();
        }

        return aDouble;
    }
}
