/*
 * NumberConversionContext.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;

import com.comact.lang.MUnit.MUnit;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2004-10-25 (16:49:36)
 * @since 1.4
 */
public class NumberConversionContext implements ObjectConversionContext
{

    public static final int OCTAL_RADIX = 8;
    public static final int DECIMAL_RADIX = 10;
    public static final int HEXADECIMAL_RADIX = 16;
    public static final int DEFAULT_RADIX = DECIMAL_RADIX;

    public static final String OCTAL_XML_RADIX_PREFIX = "0";
    public static final String DECIMAL_XML_RADIX_PREFIX = "";
    public static final String HEXADECIMAL_XML_RADIX_PREFIX = "#";
    public static final String DEFAULT_XML_RADIX_PREFIX = DECIMAL_XML_RADIX_PREFIX;

    private int radix;

    private MUnit fromMUnit;
    private MUnit toMUnit;

    public NumberConversionContext()
    {
        setRadix( DEFAULT_RADIX );
    }

    public int getRadix()
    {
        return radix;
    }

    public void setRadix( int radix )
    {
        this.radix = radix;
    }

    public String getXmlRadixPrefix()
    {
        return getXmlRadixPrefix( radix );
    }

    public static String getXmlRadixPrefix( int radix )
    {
        String radixPrefix;

        switch( radix )
        {
            case OCTAL_RADIX:
                radixPrefix = OCTAL_XML_RADIX_PREFIX;
                break;

            case HEXADECIMAL_RADIX:
                radixPrefix = HEXADECIMAL_XML_RADIX_PREFIX;
                break;

            case DECIMAL_RADIX:
                radixPrefix = DECIMAL_XML_RADIX_PREFIX;
                break;

            default:
                radixPrefix = DEFAULT_XML_RADIX_PREFIX;
                break;
        }

        return radixPrefix;
    }

    public MUnit getFromMUnit()
    {
        return fromMUnit;
    }

    public void setFromMUnit( MUnit fromMUnit )
    {
        this.fromMUnit = fromMUnit;
    }

    public MUnit getToMUnit()
    {
        return toMUnit;
    }

    public void setToMUnit( MUnit toMUnit )
    {
        this.toMUnit = toMUnit;
    }

    public boolean isRealNumberClass( Class<? extends Number> numberClass )
    {
        boolean realNumberClass = false;

        if( numberClass != null )
        {
            realNumberClass = numberClass.equals( Float.class ) || numberClass.equals( Double.class );
        }

        return realNumberClass;
    }

}
