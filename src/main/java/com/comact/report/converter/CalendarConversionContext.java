/*
 * CalendarConversionContext.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-02 (16:48:05)
 * @since 1.4
 */
public class CalendarConversionContext implements ObjectConversionContext {

	private static final String DEFAULT_BASIC_DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
	private static final String DEFAULT_BASIC_LOCALIZED_DATE_FORMAT_PATTERN = DEFAULT_BASIC_DATE_FORMAT_PATTERN + "Z";
	private static final String DEFAULT_PRECISE_DATE_FORMAT_PATTERN = DEFAULT_BASIC_DATE_FORMAT_PATTERN + ".SSS";
	private static final String DEFAULT_PRECISE_LOCALIZED_DATE_FORMAT_PATTERN = DEFAULT_PRECISE_DATE_FORMAT_PATTERN + "Z";

	private static final String DEFAULT_DATE_FORMAT_PATTERN = DEFAULT_BASIC_LOCALIZED_DATE_FORMAT_PATTERN;

	private SimpleDateFormat defaultDateFormat;
	private SimpleDateFormat basicDateFormat;
	private SimpleDateFormat basicLocalizedDateFormat;
	private SimpleDateFormat preciseDateFormat;
	private SimpleDateFormat preciseLocalizedDateFormat;

	public CalendarConversionContext() {
		setDefaultDateFormat(null);
		setBasicDateFormat(null);
		setBasicLocalizedDateFormat(null);
		setPreciseDateFormat(null);
		setPreciseLocalizedDateFormat(null);
	}

	public void setDefaultDateFormat(SimpleDateFormat defaultDateFormat) {
		if (defaultDateFormat != null) {
			this.defaultDateFormat = defaultDateFormat;
		} else if (this.defaultDateFormat == null) {
			this.defaultDateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT_PATTERN);
		}
	}

	public void setBasicDateFormat(SimpleDateFormat basicDateFormat) {
		if (basicDateFormat != null) {
			this.basicDateFormat = basicDateFormat;
		} else if (this.basicDateFormat == null) {
			this.basicDateFormat = new SimpleDateFormat(DEFAULT_BASIC_DATE_FORMAT_PATTERN);
		}
	}

	public void setBasicLocalizedDateFormat(SimpleDateFormat basicLocalizedDateFormat) {
		if (basicLocalizedDateFormat != null) {
			this.basicLocalizedDateFormat = basicLocalizedDateFormat;
		} else if (this.basicLocalizedDateFormat == null) {
			this.basicLocalizedDateFormat = new SimpleDateFormat(DEFAULT_BASIC_LOCALIZED_DATE_FORMAT_PATTERN);
		}
	}

	public void setPreciseDateFormat(SimpleDateFormat preciseDateFormat) {
		if (preciseDateFormat != null) {
			this.preciseDateFormat = preciseDateFormat;
		} else if (this.preciseDateFormat == null) {
			this.preciseDateFormat = new SimpleDateFormat(DEFAULT_PRECISE_DATE_FORMAT_PATTERN);
		}
	}

	public void setPreciseLocalizedDateFormat(SimpleDateFormat preciseLocalizedDateFormat) {
		if (preciseLocalizedDateFormat != null) {
			this.preciseLocalizedDateFormat = preciseLocalizedDateFormat;
		} else if (this.preciseLocalizedDateFormat == null) {
			this.preciseLocalizedDateFormat = new SimpleDateFormat(DEFAULT_PRECISE_LOCALIZED_DATE_FORMAT_PATTERN);
		}
	}

	public String getDefaultFormattedDateTimeString(Calendar calendar) {
		synchronized (defaultDateFormat) {
			return defaultDateFormat.format(calendar.getTime()).replaceAll("\\+0000$", "Z").replaceAll("([\\+-])(\\d\\d)(\\d\\d)$", "$1$2:$3");
		}
	}

	public String getDefaultFormattedDateTimeString(Date date) {
		synchronized (defaultDateFormat) {
			return defaultDateFormat.format(date).replaceAll("\\+0000$", "Z").replaceAll("([\\+-])(\\d\\d)(\\d\\d)$", "$1$2:$3");
		}
	}

	public String getBasicFormattedDateTimeString(Calendar calendar) {
		synchronized (basicDateFormat) {
			return basicDateFormat.format(calendar.getTime());
		}
	}

	public String getBasicLocalizedFormattedDateTimeString(Calendar calendar) {
		synchronized (basicLocalizedDateFormat) {
			return basicLocalizedDateFormat.format(calendar.getTime()).replaceAll("\\+0000$", "Z").replaceAll("([\\+-])(\\d\\d)(\\d\\d)$", "$1$2:$3");
		}
	}

	public String getPreciseFormattedDateTimeString(Calendar calendar) {
		synchronized (preciseDateFormat) {
			return preciseDateFormat.format(calendar.getTime());
		}
	}

	public String getPreciseLocalizedFormattedDateTimeString(Calendar calendar) {
		synchronized (preciseLocalizedDateFormat) {
			return preciseLocalizedDateFormat.format(calendar.getTime()).replaceAll("\\+0000$", "Z").replaceAll("([\\+-])(\\d\\d)(\\d\\d)$", "$1$2:$3");
		}
	}

	public Date getDateFromDefaultFormattedDateTimeString(String defaultFormattedDateTimeString) throws ParseException {
		synchronized (defaultDateFormat) {
			return defaultDateFormat.parse(defaultFormattedDateTimeString.replaceAll("Z$", "\\+0000").replaceAll("([\\+-])(\\d\\d):(\\d\\d)$", "$1$2$3"));
		}
	}

	public Date getDateFromBasicFormattedDateTimeString(String basicFormattedDateTimeString) throws ParseException {
		synchronized (basicDateFormat) {
			return basicDateFormat.parse(basicFormattedDateTimeString);
		}
	}

	public Date getDateFromBasicLocalizedFormattedDateTimeString(String basicFormattedDateTimeString) throws ParseException {
		synchronized (basicLocalizedDateFormat) {
			return basicLocalizedDateFormat.parse(basicFormattedDateTimeString.replaceAll("Z$", "\\+0000").replaceAll("([\\+-])(\\d\\d):(\\d\\d)$", "$1$2$3"));
		}
	}

	public Date getDateFromPreciseFormattedDateTimeString(String preciseFormattedDateTimeString) throws ParseException {
		synchronized (preciseDateFormat) {
			return preciseDateFormat.parse(preciseFormattedDateTimeString);
		}
	}

	public Date getDateFromPreciseLocalizedFormattedDateTimeString(String preciseFormattedDateTimeString) throws ParseException {
		synchronized (preciseLocalizedDateFormat) {
			return preciseLocalizedDateFormat.parse(preciseFormattedDateTimeString.replaceAll("Z$", "\\+0000").replaceAll("([\\+-])(\\d\\d):(\\d\\d)$", "$1$2$3"));
		}
	}
}
