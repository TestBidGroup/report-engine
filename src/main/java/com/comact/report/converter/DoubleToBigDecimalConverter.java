/*
 * DoubleToBigDecimalConverter.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;

import java.math.BigDecimal;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-04 (10:52:25)
 * @since 1.4
 */
public class DoubleToBigDecimalConverter extends AbstractObjectConverter<Double, BigDecimal, ObjectConversionContext>
{

    public DoubleToBigDecimalConverter() throws ObjectConverterException
    {
        super( Double.class, BigDecimal.class, null );
    }

    public BigDecimal convertObject( Double aDouble ) throws ObjectConverterException
    {
        BigDecimal aBigDecimal = null;

        if( aDouble != null )
        {
            aBigDecimal = BigDecimal.valueOf( aDouble );
        }

        return aBigDecimal;
    }
}
