/*
 * CalendarToStringConverter.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;

import java.util.Calendar;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-02 (13:35:42)
 * @since 1.4
 */
public class CalendarToStringConverter extends AbstractObjectConverter<Calendar, String, CalendarConversionContext>
{

    public CalendarToStringConverter() throws ObjectConverterException
    {
        this( null );
    }

    public CalendarToStringConverter( CalendarConversionContext calendarConversionContext ) throws ObjectConverterException
    {
        super( Calendar.class, String.class, calendarConversionContext );

    }

    public void setObjectConversionContext( CalendarConversionContext calendarConversionContext )
    {
        if( calendarConversionContext != null )
        {
            super.setObjectConversionContext( calendarConversionContext );
        }
        else
        {
            super.setObjectConversionContext( new CalendarConversionContext() );
        }
    }

    public String convertObject( Calendar calendar ) throws ObjectConverterException
    {
        String formattedDateTimeString = null;

        try
        {
            validateSrcObject( calendar );

            formattedDateTimeString = objectConversionContext.getDefaultFormattedDateTimeString( calendar );
        }
        catch( ObjectConverterException e )
        {
            throw new ObjectConverterException( ObjectConverterException.IMPOSSIBLE_TO_CONVERT_OBJECT_MSG, e );
        }

        return formattedDateTimeString;
    }
}
