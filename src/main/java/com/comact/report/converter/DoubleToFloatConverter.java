/*
 * DoubleToFloatConverter.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report.converter;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2005-03-04 (10:52:25)
 * @since 1.4
 */
public class DoubleToFloatConverter extends AbstractObjectConverter<Double, Float, ObjectConversionContext>
{

    public DoubleToFloatConverter() throws ObjectConverterException
    {
        super( Double.class, Float.class, null );
    }

    public Float convertObject( Double aDouble ) throws ObjectConverterException
    {
        Float aFloat = null;

        if( aDouble != null )
        {
            aFloat = aDouble.floatValue();
        }

        return aFloat;
    }
}
