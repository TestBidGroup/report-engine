/*
 * PairWrapper.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.report;

import java.io.Serializable;

/**
 * Type the class description here.
 *
 * @author pymathieu
 * @version 1.0, 2005-07-12 (10:36:15)
 * @since 1.4
 */
public class PairWrapper<F,S> implements Serializable
{

    protected F first = null;
    protected S second = null;

    /**
     * Pair constructor comment.
     */
    public PairWrapper()
    {
        this( null, null );
    }

    /**
     * @param first  java.lang.Object
     * @param second java.lang.Object
     */
    public PairWrapper(F first, S second )
    {
        this.first = first;
        this.second = second;
    }

    /**
     * @param newValue java.lang.Object
     */
    public void setFirst( F newValue )
    {
        this.first = newValue;
    }

    /**
     * @param newValue java.lang.Object
     */
    public void setSecond( S newValue )
    {
        this.second = newValue;
    }

    /**
     * @return java.lang.Object
     */
    public F getFirst()
    {
        return first;
    }

    /**
     * @return java.lang.Object
     */
    public S getSecond()
    {
        return second;
    }

    /**
     * @return java.lang.String
     */
    public String toString()
    {
        return "{" + getFirst() + "," + getSecond() + "}";
    }

}
