package com.comact.report;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.system.ApplicationPidFileWriter;

@SpringBootApplication(exclude = MongoAutoConfiguration.class)
    public class ReportApplication {

        public static void main(String[] args) {
            // configure listener to create PID file
            SpringApplication app = new SpringApplication(ReportApplication.class);
            app.addListeners(new ApplicationPidFileWriter());
            app.run(args);
        }
    }