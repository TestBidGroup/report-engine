package com.comact.util;

import com.comact.localization.bean.LocalizedStringBean;
import com.comact.report.bean.value.*;

/**
 * User: dlauzon
 * Date: 2017-11-13
 * Time: 09:13
 */
public class BirtUtil {
    /**
     * Méthode utilitaire pour créer une valeur par defaut des paramètre en fonction de la classe nécessaire
     * et de sa valeur en String.
     *
     * @param clazz              La classe de retour
     * @param defaultValueString La représentation String de l'object.
     * @return l'object créée dans la bonne classe.
     */
    public static RTAReportParameterValueBean createDefaultValue(Class clazz, String defaultValueString) {
        RTAReportParameterValueBean ret;


        if (Integer.class.getSimpleName().equals(clazz.getSimpleName())) {
            RTAIntegerReportParameterValueBean integerReportParameterValueBean = new RTAIntegerReportParameterValueBean();
            if (defaultValueString == null) {
                defaultValueString = "0";
            }
            integerReportParameterValueBean.setValue((Integer) initValueFromBirt(clazz, defaultValueString));
            integerReportParameterValueBean.setLabel(new LocalizedStringBean(defaultValueString));
            ret = integerReportParameterValueBean;
        } else if (Boolean.class.getSimpleName().equals(clazz.getSimpleName())) {
            RTABooleanReportParameterValueBean booleanReportParameterValueBean = new RTABooleanReportParameterValueBean();
            if (defaultValueString == null) {
                defaultValueString = "true";
            }
            booleanReportParameterValueBean.setValue((Boolean) initValueFromBirt(clazz, defaultValueString));
            booleanReportParameterValueBean.setLabel(new LocalizedStringBean(defaultValueString));
            ret = booleanReportParameterValueBean;
        } else if (Double.class.getSimpleName().equals(clazz.getSimpleName())) {
            RTADoubleReportParameterValueBean doubleReportParameterValueBean = new RTADoubleReportParameterValueBean();
            if (defaultValueString == null) {
                defaultValueString = "0.0";
            }
            doubleReportParameterValueBean.setValue((Double) initValueFromBirt(clazz, defaultValueString));
            doubleReportParameterValueBean.setLabel(new LocalizedStringBean(defaultValueString));
            ret = doubleReportParameterValueBean;
        } else {
            RTAStringReportParameterValueBean stringReportParameterValueBean = new RTAStringReportParameterValueBean();
            if (defaultValueString == null) {
                defaultValueString = "";
            }
            stringReportParameterValueBean.setValue((String) initValueFromBirt(clazz, defaultValueString));
            stringReportParameterValueBean.setLabel(new LocalizedStringBean(defaultValueString));
            ret = stringReportParameterValueBean;
        }
        return ret;
    }

    /**
     * Permet d'initialiser une valeur a partir d'une classe
     *
     * @param clazz       La classe désirer.
     * @param valueString La valeur en String
     * @return La valeur bien typer en fonction de la classe.
     */
    public static Object initValueFromBirt(Class clazz, String valueString) {
        Object ret;

        if (Integer.class.getSimpleName().equals(clazz.getSimpleName())) {
            ret = new Integer(valueString);
        } else if (Boolean.class.getSimpleName().equals(clazz.getSimpleName())) {
            ret = new Boolean(valueString);
        } else if (Double.class.getSimpleName().equals(clazz.getSimpleName())) {
            ret = new Double(valueString);
        } else {
            ret = valueString;
        }
        return ret;
    }
}
