/*
 * Milliseconds.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.util;

/**
 * Type the class description here.
 *
 * @author  slacasse
 * @version 1.0, 2003-08-18 (11:36:50)
 * @since   1.4
 */
public class Milliseconds
{

    public static final long MS_IN_A_SECOND = 1000;
    public static final long MS_IN_A_MINUTE = 60 * MS_IN_A_SECOND;
    public static final long MS_IN_A_HOUR = 60 * MS_IN_A_MINUTE;
    public static final long MS_IN_A_DAY = 24 * MS_IN_A_HOUR;
    public static final long MS_IN_A_WEEK = 7 * MS_IN_A_DAY;

    long milliseconds;

    public Milliseconds(long milliseconds )
    {
        set( milliseconds );
    }

    public long get()
    {
        return ( milliseconds );
    }

    public void set( long milliseconds )
    {
        this.milliseconds = milliseconds;
    }

    public double getSeconds()
    {
        return ( ( double ) this.milliseconds / MS_IN_A_SECOND );
    }

    public void setFromSeconds( double nbSeconds )
    {
        this.milliseconds = ( long ) ( nbSeconds * ( double ) MS_IN_A_SECOND );
    }

    public double getMinutes()
    {
        return ( ( double ) this.milliseconds / MS_IN_A_MINUTE );
    }

    public void setFromMinutes( double nbMinutes )
    {
        this.milliseconds = ( long ) ( nbMinutes * ( double ) MS_IN_A_MINUTE );
    }

    public double getHours()
    {
        return ( ( double ) this.milliseconds / MS_IN_A_HOUR );
    }

    public void setFromHours( double nbHours )
    {
        this.milliseconds = ( long ) ( nbHours * ( double ) MS_IN_A_HOUR );
    }

    public double getDays()
    {
        return ( ( double ) this.milliseconds / MS_IN_A_DAY );
    }

    public void setFromDays( double nbDays )
    {
        this.milliseconds = ( long ) ( nbDays * ( double ) MS_IN_A_DAY );
    }

    public double getWeeks()
    {
        return ( ( double ) this.milliseconds / MS_IN_A_WEEK );
    }

    public void setFromWeeks( double nbWeeks )
    {
        this.milliseconds = ( long ) ( nbWeeks * ( double ) MS_IN_A_WEEK );
    }

    public static synchronized String getExpression( long milliseconds )
    {
        return ( milliseconds + " ms." );
    }

    public static synchronized String getSecondsExpression( long milliseconds )
    {
        return ( ( milliseconds / MS_IN_A_SECOND ) + " sec." );
    }

    public static synchronized String getMinutesExpression( long milliseconds )
    {
        return ( ( milliseconds / MS_IN_A_MINUTE ) + " min." );
    }

    public static synchronized String getHoursExpression( long milliseconds )
    {
        return ( ( milliseconds / MS_IN_A_HOUR ) + " hours" );
    }

    public static synchronized String getDaysExpression( long milliseconds )
    {
        return ( ( milliseconds / MS_IN_A_DAY ) + " days" );
    }

    public static synchronized String getWeeksExpression( long milliseconds )
    {
        return ( ( milliseconds / MS_IN_A_WEEK ) + " weeks" );
    }

    public static synchronized String getShortestExpression( long milliseconds )
    {
        String shortestExpression = null;
        if( milliseconds > MS_IN_A_WEEK )
        {
            shortestExpression = getWeeksExpression( milliseconds );
        }
        else if( milliseconds > MS_IN_A_DAY )
        {
            shortestExpression = getDaysExpression( milliseconds );
        }
        else if( milliseconds > MS_IN_A_HOUR )
        {
            shortestExpression = getHoursExpression( milliseconds );
        }
        else if( milliseconds > MS_IN_A_MINUTE )
        {
            shortestExpression = getMinutesExpression( milliseconds );
        }
        else if( milliseconds > MS_IN_A_SECOND )
        {
            shortestExpression = getSecondsExpression( milliseconds );
        }
        else
        {
            shortestExpression = getExpression( milliseconds );
        }

        return shortestExpression;
    }

    public String toString()
    {
        return getShortestExpression( this.milliseconds );
    }
}
