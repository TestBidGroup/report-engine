/*
 * DurationTimer.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.comact.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Type the class description here.
 *
 * @author Author
 * @version 1.0, 2003-05-08 (13:32:00)
 * @since 1.4
 */
public class DurationTimer
{

    private static final Logger logger = LoggerFactory.getLogger( DurationTimer.class );

    private static final TimeUnit DEFAULT_TIME_UNIT = TimeUnit.NANOSECONDS;
    private static final int DEFAULT_MAXIMUM_NUMBER_OF_SAMPLES = 50;

    private String name;

    private boolean running;

    private long startTime;
    private long elapsedTime;

    private DataSampler dataSampler;

    private Alarm alarm;

    private static HashMap<String, DurationTimer> durationTimers;

    /**
     *
     */
    public DurationTimer()
    {
        this( null, DEFAULT_MAXIMUM_NUMBER_OF_SAMPLES );
    }

    public DurationTimer(String name )
    {
        this( name, DEFAULT_MAXIMUM_NUMBER_OF_SAMPLES );
    }

    /**
     * @param name
     */
    public DurationTimer(String name, int maximumNuberOfSamples )
    {
        this( name, maximumNuberOfSamples, null );
    }

    public DurationTimer(String name, int maximumNuberOfSamples, Alarm alarm )
    {
        this.name = name;
        dataSampler = new DataSampler( maximumNuberOfSamples );
        this.alarm = alarm;
        reset();
    }

    public void reset()
    {
        running = false;
        elapsedTime = 0;
        dataSampler.clear();
    }

    /**
     *
     */
    public void start()
    {
        elapsedTime = 0;
        startTime = System.nanoTime();
        running = true;
    }

    /**
     *
     */
    public void pause()
    {
        if( running == true )
        {
            elapsedTime += ( System.nanoTime() - startTime );
            running = false;
        }
    }

    /**
     *
     */
    public void resume()
    {
        if( running == false )
        {
            startTime = System.nanoTime();
            running = true;
        }
    }

    /**
     * @return
     */
    public long stop()
    {
        if( running == true )
        {
            elapsedTime += ( System.nanoTime() - startTime );

            running = false;

            dataSampler.add( elapsedTime );
        }
        return ( elapsedTime );
    }

    public boolean isRunning()
    {
        return ( running == true );
    }

    public String getName()
    {
        return ( this.name );
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public long getElapsedTime()
    {
        return getElapsedTime( TimeUnit.MILLISECONDS );
    }

    public long getElapsedTime( TimeUnit timeUnit )
    {
        long elapsedTime = this.elapsedTime;

        if( running == true )
        {
            elapsedTime += ( System.nanoTime() - startTime );
        }

        return ( timeUnit.convert( elapsedTime, DEFAULT_TIME_UNIT ) );
    }

    public long getMinElapsedTime()
    {
        return getMinElapsedTime( TimeUnit.MILLISECONDS );
    }

    public long getMinElapsedTime( TimeUnit timeUnit )
    {
        return timeUnit.convert( ( long ) dataSampler.getMin(), DEFAULT_TIME_UNIT );
    }

    public long getMaxElapsedTime()
    {
        return getMaxElapsedTime( TimeUnit.MILLISECONDS );
    }

    public long getMaxElapsedTime( TimeUnit timeUnit )
    {
        return timeUnit.convert( ( long ) dataSampler.getMax(), DEFAULT_TIME_UNIT );
    }

    public long getAvgElapsedTime()
    {
        return getAvgElapsedTime( TimeUnit.MILLISECONDS );
    }

    public long getAvgElapsedTime( TimeUnit timeUnit )
    {
        return timeUnit.convert( ( long ) dataSampler.getAvg(), DEFAULT_TIME_UNIT );
    }

    public Alarm getAlarm()
    {
        return alarm;
    }

    public void setAlarm( Alarm alarm )
    {
        this.alarm = alarm;
    }

    public void checkAndLogAlarms()
    {
        if( alarm != null )
        {
            alarm.checkAndLogAlarms( this );
        }
    }

    public void checkAndLogAlarms( Logger logger )
    {
        if( alarm != null )
        {
            alarm.checkAndLogAlarms( this, logger );
        }
    }

    public void toConsole()
    {
        System.out.println( toStringInMills() );
    }

    /**
     * @return
     */
    public String toStringInMills()
    {
        return ( "DurationTimer (" + name +
                 ") Elapsed = " + getElapsedTime() +
                 ", NbSamples = " + dataSampler.getNumberOfSamples() +
                 ", Min. = " + getMinElapsedTime() +
                 ", Max. = " + getMaxElapsedTime() +
                 ", Avg. = " + getAvgElapsedTime() );
    }

    public String toString()
    {
        return ( "DurationTimer (" + name +
                 ") Elapsed = " + Milliseconds.getShortestExpression( getElapsedTime() ) +
                 ", NbSamples = " + dataSampler.getNumberOfSamples() +
                 ", Min. = " + Milliseconds.getShortestExpression( getMinElapsedTime() ) +
                 ", Max. = " + Milliseconds.getShortestExpression( getMaxElapsedTime() ) +
                 ", Avg. = " + Milliseconds.getShortestExpression( getAvgElapsedTime() ) );
    }

    public static synchronized DurationTimer getMappedDurationTimer( String name )
    {
        return getMappedDurationTimer( name, DEFAULT_MAXIMUM_NUMBER_OF_SAMPLES );
    }

    public static synchronized DurationTimer getMappedDurationTimer( String name, int maximumNuberOfSamples )
    {
        return getMappedDurationTimer( name, maximumNuberOfSamples, null );
    }

    public static synchronized DurationTimer getMappedDurationTimer( String name, int maximumNuberOfSamples, Alarm alarm )
    {
        if( ( name == null ) || ( name.length() < 1 ) )
        {
            throw new IllegalArgumentException( "You must provide a valid name in order to get a mapped duration timer." );
        }

        DurationTimer durationTimer;

        if( durationTimers == null )
        {
            durationTimers = new HashMap<String, DurationTimer>();
        }

        durationTimer = durationTimers.get( name );

        if( durationTimer == null )
        {
            durationTimer = new DurationTimer( name, maximumNuberOfSamples, alarm );
            durationTimers.put( name, durationTimer );
        }

        return durationTimer;
    }

    public static synchronized DurationTimer unmapDurationTimer( String name )
    {
        DurationTimer removedDurationTimer = null;

        if( ( durationTimers != null ) && ( name != null ) )
        {
            removedDurationTimer = durationTimers.remove( name );
        }

        return removedDurationTimer;
    }

    public class Alarm
    {

        private Logger logger;

        private long warnLevelInNanoseconds;
        private long alarmLevelInNanoseconds;

        public Alarm( long alarmLevelInNanoseconds )
        {
            this( alarmLevelInNanoseconds, DurationTimer.logger );
        }

        public Alarm( long alarmLevelInNanoseconds, Logger logger )
        {
            this( Long.MIN_VALUE, alarmLevelInNanoseconds, logger );
        }

        public Alarm( long warnLevelInNanoseconds, long alarmLevelInNanoseconds )
        {
            this( warnLevelInNanoseconds, alarmLevelInNanoseconds, DurationTimer.logger );
        }

        public Alarm( long warnLevelInNanoseconds, long alarmLevelInNanoseconds, Logger logger )
        {
            setWarnLevelInNanoseconds( warnLevelInNanoseconds );
            setAlarmLevelInNanoseconds( alarmLevelInNanoseconds );
            setLogger( logger );
        }

        public long getWarnLevelInNanoseconds()
        {
            return warnLevelInNanoseconds;
        }

        public void setWarnLevelInNanoseconds( long warnLevelInNanoseconds )
        {
            this.warnLevelInNanoseconds = warnLevelInNanoseconds;
        }

        public long getAlarmLevelInNanoseconds()
        {
            return alarmLevelInNanoseconds;
        }

        public void setAlarmLevelInNanoseconds( long alarmLevelInNanoseconds )
        {
            this.alarmLevelInNanoseconds = alarmLevelInNanoseconds;
        }

        public Logger getLogger()
        {
            return logger;
        }

        public void setLogger( Logger logger )
        {
            this.logger = logger;
        }

        public void checkAndLogAlarms( DurationTimer durationTimer )
        {
            checkAndLogAlarms( durationTimer, logger );
        }

        public void checkAndLogAlarms( DurationTimer durationTimer, Logger logger )
        {
            if( durationTimer.getElapsedTime() > alarmLevelInNanoseconds )
            {
                logger.error( DurationTimer.class.getSimpleName() + " (" + durationTimer.getName() + ") has reached alarm level!" );
            }
            else if( durationTimer.getElapsedTime() > warnLevelInNanoseconds )
            {
                logger.error( DurationTimer.class.getSimpleName() + " (" + durationTimer.getName() + ") has reached warn level!" );
            }
        }

    }
}
