/*
 * NumberUtils.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.util;

/**
 * User: csavard
 * Date: 11-10-24
 * Time: 16:10
 */
public class NumberUtils {

    public static final int NBCOUNTER_IN_MILLI = 10000;
    private static long counter;

    public static boolean[] toBitArray(int value) {
        boolean[] retValue = new boolean[32];
        String stringValue = Integer.toBinaryString(value);
        char[] binaryString = new StringBuilder(stringValue).reverse().toString().toCharArray();

        for (int i = 0; i < binaryString.length; i++) {
            char c = binaryString[i];
            if (c == '1') {
                retValue[i] = true;
            }
        }


        return retValue;
    }

    public static long generateUniqueId() {
        long id = (System.currentTimeMillis() * NBCOUNTER_IN_MILLI) + counter++;
        counter = counter % NBCOUNTER_IN_MILLI;
        return id;
    }

}
