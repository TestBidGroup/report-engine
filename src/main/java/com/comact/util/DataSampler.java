/*
 * DataSampler.java
 *
 * Copyright (c) 2012 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.comact.util;

import java.io.Serializable;
import java.util.Vector;

/**
 * Type the class description here.
 *
 * @author slacasse
 * @version 1.0, 2003-08-15 (14:43:49)
 * @since 1.4
 */
public class DataSampler implements Serializable {
    private double min;
    private double max;
    private double sum;

    private int maximumNumberOfSamples;

    private Vector<Double> samples;

    public DataSampler(int maximumNumberOfSamples) {
        if (maximumNumberOfSamples < 2) {
            maximumNumberOfSamples = 2;
        }

        this.maximumNumberOfSamples = maximumNumberOfSamples;

        samples = new Vector<Double>(maximumNumberOfSamples);

        clear();
    }

    public void clear() {
        samples.clear();

        min = Double.POSITIVE_INFINITY;
        max = Double.NEGATIVE_INFINITY;

        sum = 0.0d;
    }

    public void add(double value) {
        while (samples.size() >= maximumNumberOfSamples) {
            sum -= samples.remove(0);
        }

        samples.add(value);
        sum += value;

        if (value < min) {
            min = value;
        }
        if (value > max) {
            max = value;
        }
    }

    public int getNumberOfSamples() {
        return samples.size();
    }

    public double getMin() {
        if (getNumberOfSamples() == 0) {
            return 0.0f;
        } else {
            return min;
        }
    }

    public double getMax() {
        if (getNumberOfSamples() == 0) {
            return 0.0f;
        } else {
            return max;
        }
    }

    public double getAvg() {
        if (getNumberOfSamples() == 0) {
            return 0.0f;
        } else {
            return sum / getNumberOfSamples();
        }
    }

    public Vector<Double> getSamples() {
        return samples;
    }

}
