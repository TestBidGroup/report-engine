/*
 * RTA_Lib.js
 *
 * Copyright (c) 2014 Comact Optimisation, Inc. All rights reserved.
 * COMACT PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/*function setParamsNoTemporal(queryText, params)
 {
 queryText =  setParams(queryText, params, false);
 return queryText;
 }

 function setParamsTemporal(queryText, params){

 queryText =  setParams(queryText, params, true);	
 return queryText;
 }*/

function formatDataCell(cell) {
	if (cell.getValue() < 10) {

		cell.getStyle().numberFormat = "#.###";
	} else if (cell.getValue() < 100) {
		cell.getStyle().numberFormat = "#.##";
	} else if (cell.getValue() < 1000) {
		cell.getStyle().numberFormat = "#.#";
	} else {
		cell.getStyle().numberFormat = "###,###";
	}
}

function formatTime(timeInSecond) {
	var ret = "";
	var newTime = timeInSecond;
	if (timeInSecond > 86400) {
		var days = Math.floor(timeInSecond / 86400);
		ret = ret + days + " "
				+ reportContext.getMessage("Days", reportContext.getLocale())
				+ " ";
		newTime = timeInSecond - (days * 86400);
	}
	var date = new Date(0, 0, 0, 0, 0, 0, newTime * 1000);
	ret = ret + date.toTimeString().substring(0, 8);
	return ret;
}

/**
 * Permet de formatter une date qui possède aussi du temps et en fonction du
 * type demandé on affiche seulement le temps ou seulement la date
 * 
 * @param date
 *            La date a affiché
 * @param timeInterval
 *            l'interval de temps que l'on a choisi
 * @returns Une string représentant la date.
 */
function formatDateTime(date, timeInterval) {

	if ("MINUTE".equals(timeInterval) || "HOUR".equals(timeInterval)) {
		var hours = addPaddingFront(date.getHours(), "00");
		var minutes = addPaddingFront(date.getMinutes(), "00");
		var seconds = addPaddingFront(date.getSeconds(), "00");

		return hours + ":" + minutes + ":" + seconds;
	} else {
		// var locale = reportContext.getLocale().toString();
		var days = addPaddingFront(date.getDate(), "00");
		var months = addPaddingFront(date.getMonth() + 1, "00");
		var years = date.getFullYear();

		return days + "-" + months + "-" + years;
	}
}

function hideDebugInReports(){
	return ("HideDebug" in params
		&& params["HideDebug"] != null
		&& params["HideDebug"]);
};

/**
 * Ajoute du padding au debut du champs recu
 * 
 * @param str
 *            Le champ pour lequel on veut ajouter du padding
 * @param padding
 *            Le Padding que l'on veut ajouter au debut
 * @returns Une chaine de charactère paddée
 */
function addPaddingFront(str, padding) {
	var temp = padding + str;
	return temp.substring(temp.length - padding.length, temp.length);
}

function isParamSet(paramName, params) {
	var ret = false;

	if (paramName in params && params[paramName] != null) {
		if (isNaN(params[paramName]))
			ret = !"".equals(params[paramName])
					&& !"null".equals(params[paramName]);
		else
			ret = params[paramName] > 0;
	}

	return ret;
}

function setParams(queryText, params) {
	return setParams(queryText, params, "", "");
}

function isLinear() {
	var linear = false;
	switch (params["MachineName"].value) {
	case "LogSorter":
	case "DDM":
	case "LinEdger":
	case "TBL":
	case "OLI":
	case "Vision_Alone":
	case "Bucking":
	case "EndDogging":
		linear = true;
		break;
	}

	return linear;
}



/*if ("metric" in params && params["metric"] == true) {
queryText = queryText.replaceAll("__I3_TO_CURRENT__",
		"(1 / (144 * 423.7))");
queryText = queryText.replaceAll("__INCH_TO_CURRENT__", "(2.54)");
queryText = queryText.replaceAll("__INCH_TO_LENGTH__", "(2.54)/100");
queryText = queryText.replaceAll("__INCH_TO_CURRENT_MM__",
		"(2.54 * 10)");
queryText = queryText.replaceAll("__LENGTH_TO_CURRENT__", "0.3048");
queryText = queryText.replaceAll("__SWEEP_TO_CURRENT__", "25.4");
queryText = queryText.replaceAll("__SWEEP_INTERVAL__", "1");
queryText = queryText.replaceAll("__TAPER_TO_CURRENT__", "1000");
queryText = queryText.replaceAll("__TAPER_INTERVAL__", "1");
queryText = queryText.replaceAll("__TAPER_LENGTH_TO_CURRENT__",
		"39.3700788");
queryText = queryText.replaceAll("__CUT_INTERVAL__", "10");
queryText = queryText.replaceAll("__DENSITY_TO_CURRENT__", "1")
} else {
queryText = queryText.replaceAll("__I3_TO_CURRENT__", "(1 / 144)");
queryText = queryText.replaceAll("__INCH_TO_CURRENT__", "1");
queryText = queryText.replaceAll("__INCH_TO_CURRENT_MM__", "1");
queryText = queryText.replaceAll("__INCH_TO_LENGTH__", "1/12");
queryText = queryText.replaceAll("__LENGTH_TO_CURRENT__", "1");
queryText = queryText.replaceAll("__SWEEP_TO_CURRENT__", "1");
queryText = queryText.replaceAll("__SWEEP_INTERVAL__", ".25");
queryText = queryText.replaceAll("__TAPER_TO_CURRENT__", "12");
queryText = queryText.replaceAll("__TAPER_LENGTH_TO_CURRENT__", "12");
queryText = queryText.replaceAll("__TAPER_INTERVAL__", ".025");
queryText = queryText.replaceAll("__DENSITY_TO_CURRENT__", "1");

if ("Dimension_Raw" in params && params["Dimension_Raw"] != null
		&& params["Dimension_Raw"] == 0) {
	queryText = queryText.replaceAll("__CUT_INTERVAL__", "12");
} else {
	queryText = queryText.replaceAll("__CUT_INTERVAL__", "10");
}
}*/
function i3ToCurrent(value){
	if ("metric" in params && params["metric"] == true) {
		return value * (1 / (144 * 423.7));
	}else{
		return value * (1 / 144);
	}
}

function i3ToCurrentInput(value){
	if ("input-metric" in params && params["input-metric"] == true) {
		return value * (1/(144 * 423.7));
	} else {
		return value * (1 / 1728);
	}
}

/**
 * Retourne si le paramètres Metric est on.
 * @returns
 */
function isMetric(){
	return "metric" in params && params["metric"] == true;
}

/**
 * Retorune si le Input Metric est a on.
 * @returns
 */
function isInputMetric(){
	return "input-metric" in params && params["input-metric"] == true;
}

function setParams(queryText, params, addToWhere, addToJoin) { // ,
																// isTemporal){
	var where = " where 1 = 1";
	var join = "";
	var where_sub_batch = " where 1 = 1";
	var where_downtime = " where 1 = 1";
	var where_batch_report = " where 1 = 1";

	var currentLanguage = reportContext.getLocale().toString().substr(0, 2);

	if (addToWhere != null) {

		where = where + addToWhere;
	}
	if (addToJoin != null) {
		join = join + addToJoin;
	}
	
	if(queryText.contains("WEIGHT__")){
		join = join + " LEFT JOIN report_species_density species_density ON sol.specie = species_density.code ";
		if(queryText.contains("__WEIGHT__")) {
			queryText = queryText.replaceAll("__WEIGHT__", " IFNULL(sum(species_density.density * __DENSITY_TO_CURRENT__ * sol.volume * 1/(1728)), 0)");
		}
		
		if(queryText.contains("__STEMS_WEIGHT__")) {
			queryText = queryText.replaceAll("__STEMS_WEIGHT__", " IFNULL(sum(species_density.density * __DENSITY_TO_CURRENT__ * sol.volume * 1/(1728)), 0)");
		}
	}

	queryText = queryText.replaceAll("__LOCALIZED_TEXT__", "text_"
			+ currentLanguage);
	queryText = queryText.replaceAll("__NO_COLORMARK__", "\""
			+ reportContext.getMessage("No ColorMark", reportContext
					.getLocale()) + "\"");

	if (params["MachineName"].value == "Bucking") {
		queryText = queryText
				.replaceAll(
						"__JOIN_BOARDS__",
						" INNER JOIN report_log log on sol.id = log.report_solution "
								+ " INNER JOIN report_solution_board boards on boards.report_log = log.id ");

		queryText = queryText
				.replaceAll(
						"__LEFT_JOIN_BOARDS__",
						" LEFT JOIN report_log log ON sol.id = log.report_solution "
								+ " LEFT JOIN report_solution_board boards ON boards.report_log = log.id");

		queryText = queryText.replaceAll("__JOIN_LOGS__",
				" INNER JOIN report_log log on sol.id = log.report_solution ");

		queryText = queryText.replaceAll("__LOG__", "log");

		if ("SubMachine" in params && params["SubMachine"] != null
				&& params["SubMachine"] >= 0) {
			queryText = queryText.replaceAll("__LOG_SUB_MACHINE__", "log");
			queryText = queryText
					.replaceAll("__JOIN_LOG_SUB_MACHINE__",
							"report_solution sol INNER JOIN report_log log on log.report_solution = sol.id");
			if (params["SubMachine"] > 0) {
				where = where + " and log.index_machine = "
						+ params["SubMachine"];
			}

			if ("Data" in params && params["Data"] != null) {
				if (params["Data"] == 0) {
					queryText = queryText.replaceAll("__DATA_TO_SELECT__",
							"count(sol.id)");

					queryText = queryText.replaceAll(
							"__BOARD_DATA_TO_SELECT__", "count(boards.id)");

					queryText = queryText.replaceAll(
							"__CONDITION_NUMBER_VOLUME__", "then 1 else 0");
				} else if (params["Data"] == 1) {
					queryText = queryText
							.replaceAll("__DATA_TO_SELECT__",
									"sum(sol.volume * __I3_TO_CURRENT_INPUT__ * __LARGE_SCALE_COEFFICIENT__ )");

					queryText = queryText
							.replaceAll("__BOARD_DATA_TO_SELECT__",
									"sum(thickness.nominal * width.nominal * length.nominal) * __I3_TO_CURRENT__ ");

					queryText = queryText
							.replaceAll(
									"__CONDITION_NUMBER_VOLUME__",
									"then sol.volume * __I3_TO_CURRENT_INPUT__ * __LARGE_SCALE_COEFFICIENT__  else 0");
				}
			}

		} else {
			queryText = queryText.replaceAll("__LOG_SUB_MACHINE__", "sol");
			queryText = queryText.replaceAll("__JOIN_LOG_SUB_MACHINE__",
					"report_solution sol");
		}
	} else {
		queryText = queryText
				.replaceAll("__JOIN_BOARDS__",
						" INNER JOIN report_solution_board boards on boards.report_solution = sol.id ");

		queryText = queryText
				.replaceAll("__LEFT_JOIN_BOARDS__",
						" LEFT JOIN report_solution_board boards ON boards.report_solution = sol.id ");

		queryText = queryText.replaceAll("__JOIN_LOGS__", "");

		queryText = queryText.replaceAll("__LOG__", "sol");
		queryText = queryText.replaceAll("__LOG_SUB_MACHINE__", "sol");

		queryText = queryText.replaceAll("__JOIN_LOG_SUB_MACHINE__",
				"report_solution sol");

		if ("SubMachine" in params && params["SubMachine"] != null
				&& params["SubMachine"] > 0) {
			where = where + " and sol.index_machine = " + params["SubMachine"];
		}
	}

	if ("ShiftId" in params && params["ShiftId"] != null
			&& params["ShiftId"] > 0) {

		if (this.queryText.contains("report_lugs lug")) {
			where = where + " and lug.shift = " + params["ShiftId"];
		} else if (this.queryText.contains("report_chain_displacement disp")) {
			where = where + " and disp.shift = " + params["ShiftId"];
		} else {
			where = where + " and sol.shift = " + params["ShiftId"];
			where_sub_batch = where_sub_batch + " and rsb.report_shift = "
					+ params["ShiftId"];
			where_downtime = where_downtime + " and downtime.report_shift = "
					+ params["ShiftId"];

			queryText = queryText.replaceAll("__BEGIN_DATE__",
					"(SELECT shift.start_date FROM report_shift shift where shift.id = "
							+ params["ShiftId"] + " )");
			queryText = queryText.replaceAll("__END_DATE__",
					"(SELECT shift.end_date FROM report_shift shift where shift.id = "
							+ params["ShiftId"] + " )");

			queryText = queryText.replaceAll("__BEGIN_BATCH__",
					"(SELECT shift.start_date FROM report_shift shift where shift.id = "
							+ params["ShiftId"] + " )");
			queryText = queryText.replaceAll("__END_BATCH__",
					"(SELECT shift.end_date FROM report_shift shift where shift.id = "
							+ params["ShiftId"] + " )");
		}
	}

	/*
	 * if("Provider" in params && params["Provider"].value != null &&
	 * params["Provider"].value != "null" && params["Provider"].value != "" ){
	 * join = join + " inner join provider prov on prov.id = sol.provider ";
	 * join = join + " inner join provider_definition provDef on provDef.id =
	 * prov.provider_definition ";
	 * 
	 * where = where + " and provDef.name like '" + params["Provider"] + "' " ; }
	 */

	if ("Provider" in params && params["Provider"].value != null
			&& params["Provider"].value != null
			&& params["Provider"].value.length > 0
			&& params["Provider"].value[0] != null
			&& params["Provider"].value[0] != ""
			&& params["Provider"].value[0] != 0) {
		if (params["Provider"].value.length > 0) {
			join = join
					+ " inner join provider prov on prov.id = sol.provider ";

			where = where + " and prov.provider_definition IN ("
					+ params["Provider"].value.join(",") + ") ";
		}
	}

	if ("BinNumber" in params && params["BinNumber"] != null
			&& params["BinNumber"].value != null
			&& params["BinNumber"].value.length > 0
			&& params["BinNumber"].value[0] != null
			&& params["BinNumber"].value[0] != ""
			&& params["BinNumber"].value[0] != 0) {
		if (params["BinNumber"].value.length > 0) {
			where = where + " and sol.bin IN ("
					+ params["BinNumber"].value.join(",") + ") ";
		}
	}

	if ("BeginDate" in params && params["BeginDate"] != null
			&& !"null".equals(params["BeginDate"])) {
		if (queryText.contains("__BEGIN_DATE__")) {
			queryText = queryText.replaceAll("__BEGIN_DATE__", "'"
					+ params["BeginDate"] + "'");
		}

		if (queryText.contains("__BEGIN_BATCH__")) {
			queryText = queryText.replaceAll("__BEGIN_BATCH__", "'"
					+ params["BeginDate"] + "'");
		}

		if (this.queryText.contains("report_lugs lug")) {
			where = where + " and lug.date >= '" + params["BeginDate"] + "'";
		} else if (this.queryText.contains("report_chain_displacement disp")) {
			where = where + " and disp.date >= '" + params["BeginDate"] + "'";
		} else {
			where = where + " and sol.date >= '" + params["BeginDate"] + "'";
			where_sub_batch = where_sub_batch + " and rsb.start_date >= '"
					+ params["BeginDate"] + "'";

			// ici pour les downtimes on assume que on va avoir un BeginDate et
			// un EndDate.
			where_downtime = where_downtime + " AND ( '" + params["BeginDate"]
					+ "' BETWEEN shift.start_date AND shift.end_date ";
			where_downtime = where_downtime + " OR '" + params["EndDate"]
					+ "' BETWEEN shift.start_date AND shift.end_date ) ";
		}
	}
	if ("TypeOpti" in params && params["TypeOpti"] != null
			&& !"null".equals(params["TypeOpti"])) {
		if (!this.queryText.contains("report_lugs lug")
				&& !this.queryText.contains("report_chain_displacement disp")) {
			where = where + " and sol.scannerid = " + params["TypeOpti"];
		}
	}
	if ("ScanMode" in params && params["ScanMode"] != null
			&& params["ScanMode"].value > 0) {
		if (this.queryText.contains("report_lugs lug")) {

		} else {
			where = where + " and sol.opti_piece_in_piece = ";
			if (params["ScanMode"].value == 1) {
				where = where + "1 ";
			} else {
				where = where + "0 ";
			}
		}

	}
	if ("EndDate" in params && params["EndDate"] != null
			&& !"null".equals(params["EndDate"])) {
		if (queryText.contains("__END_DATE__")) {
			queryText = queryText.replaceAll("__END_DATE__", "'"
					+ params["EndDate"] + "'");
		}

		if (queryText.contains("__END_BATCH__")) {
			queryText = queryText.replaceAll("__END_BATCH__", "'"
					+ params["EndDate"] + "'");
		}

		if (this.queryText.contains("report_lugs lug")) {
			where = where + " and lug.date <= '" + params["EndDate"] + "'";
		} else if (this.queryText.contains("report_chain_displacement disp")) {
			where = where + " and disp.date <= '" + params["EndDate"] + "'";
		} else {
			where = where + " and sol.date <= '" + params["EndDate"]
					+ "' and sol.live = 1";
			where_sub_batch = where_sub_batch + " and rsb.end_date <= '"
					+ params["EndDate"] + "' ";
		}
	}

	if (!this.queryText.contains("report_lugs lug")
			&& !this.queryText.contains("report_chain_displacement disp")) {
		if ("BatchId" in params && params["BatchId"] != null
				&& params["BatchId"].value != null
				&& params["BatchId"].value.length > 0
				&& params["BatchId"].value[0] != null
				&& params["BatchId"].value[0] != ""
				&& params["BatchId"].value[0] != 0) {
			where = where + " and sol.report_batch IN ("
					+ params["BatchId"].value.join(",") + ") ";
			where_sub_batch = where_sub_batch + " and rsb.report_batch IN ("
					+ params["BatchId"].value.join(",") + ") ";

			queryText = queryText.replaceAll("__BEGIN_BATCH__", "null");
			queryText = queryText.replaceAll("__END_BATCH__", "null");

		} else if ("BatchDefinitionId" in params
				&& params["BatchDefinitionId"] != null
				&& !"".equals(params["BatchDefinitionId"])
				&& !"null".equals(params["BatchDefinitionId"])
				&& !"0".equals(params["BatchDefinitionId"])) {
			join = join
					+ " inner join report_batch batch on batch.id = sol.report_batch ";
			join = join
					+ " inner join batch_definition_link batch_def_link on batch_def_link.child = batch.batch_definition and batch_def_link.parent = "
					+ params["BatchDefinitionId"] + " ";

			where_sub_batch = where_sub_batch + " and bd.id ="
					+ params["BatchDefinitionId"];
			where_batch_report = where_batch_report + " and 1 = 0";
		}
	}

	if (queryText.contains("__REJECT__")) {
		queryText = queryText.replaceAll("__REJECT__", "\""
				+ reportContext.getMessage("Reject", reportContext.getLocale())
				+ "\"");
	}

	if (queryText.contains("__OTHER__")) {
		queryText = queryText.replaceAll("__OTHER__", "\""
				+ reportContext.getMessage("Other", reportContext.getLocale())
				+ "\"");
	}

	if (queryText.contains("__OPTI_REJECT__")) {
		if ("EnableFinalPreOpti" in params
				&& !(params["EnableFinalPreOpti"].value)) {
			queryText = queryText
					.replaceAll(
							"__OPTI_REJECT__",
							",IFNULL( count( Case When sol.optimization=1 and sol.total_value=0 then sol.id end ),0 ) as TotalNbLogsRejected \n"
									+ ",IFNULL( sum( Case When sol.optimization=1 and sol.total_value=0 then sol.volume* __I3_TO_CURRENT_INPUT__ end ),0 ) * __LARGE_SCALE_COEFFICIENT__ as TotalVolumeOfLogsRejected \n"
									+ ",IFNULL( FLOOR(sum( Case When sol.optimization=1 and sol.total_value=0 then sol.length  end )* __INCH_TO_LENGTH__) ,0 ) * __LARGE_SCALE_COEFFICIENT__ as TotalLinearLengthRejected \n"
									+ ",IFNULL( avg( Case When sol.optimization=1 and sol.total_value=0 then sol.volume* __I3_TO_CURRENT_INPUT__ end ),0 ) as TotalAvgVolumeRejected \n"
									+ ",IFNULL( avg( Case When sol.optimization=1 and sol.total_value=0 then sol.length * __INCH_TO_LENGTH__ end ),0 ) as TotalAvgLengthRejected \n"
									+ ",IFNULL( avg( Case When sol.optimization=1 and sol.total_value=0 then sol.diameter * __INCH_TO_CURRENT__ end ),0 ) as TotalAvgDiameterRejected \n");
		} else {
			queryText = queryText
					.replaceAll(
							"__OPTI_REJECT__",
							",IFNULL( count( Case When sol.optimization=1 then sol.id end ),0 ) as TotalNbLogsRejected \n"
									+ ",IFNULL( sum( Case When sol.optimization=1 then sol.volume* __I3_TO_CURRENT_INPUT__ end ),0 ) * __LARGE_SCALE_COEFFICIENT__ as TotalVolumeOfLogsRejected \n"
									+ ",IFNULL( FLOOR(sum( Case When sol.optimization=1 then sol.length  end )* __INCH_TO_LENGTH__) ,0 ) * __LARGE_SCALE_COEFFICIENT__ as TotalLinearLengthRejected \n"
									+ ",IFNULL( avg( Case When sol.optimization=1 then sol.volume* __I3_TO_CURRENT_INPUT__ end ),0 ) as TotalAvgVolumeRejected \n"
									+ ",IFNULL( avg( Case When sol.optimization=1 then sol.length * __INCH_TO_LENGTH__ end ),0 ) as TotalAvgLengthRejected \n"
									+ ",IFNULL( avg( Case When sol.optimization=1 then sol.diameter * __INCH_TO_CURRENT__ end ),0 ) as TotalAvgDiameterRejected \n");
		}
	}

	if (queryText.contains("__OPTI_SLI__")) {
		if ("EnableFinalPreOpti" in params
				&& !(params["EnableFinalPreOpti"].value)) {
			queryText = queryText
					.replaceAll(
							"__OPTI_SLI__",
							",IFNULL( count( Case When sol.optimization=1 and sol.total_value!=0 then sol.id end ),0 ) as TotalNbLogsSLI \n"
									+ ",IFNULL( sum( Case When sol.optimization=1 and sol.total_value!=0 then sol.volume* __I3_TO_CURRENT_INPUT__ end ),0 ) * __LARGE_SCALE_COEFFICIENT__ as TotalVolumeOfLogsSLI \n"
									+ ",IFNULL( FLOOR(sum( Case When sol.optimization=1 and sol.total_value!=0 then sol.length end ) * __INCH_TO_LENGTH__),0 ) * __LARGE_SCALE_COEFFICIENT__ as TotalLinearLengthSLI \n"
									+ ",IFNULL( avg( Case When sol.optimization=1 and sol.total_value!=0 then sol.volume* __I3_TO_CURRENT_INPUT__ end ),0 ) as TotalAvgVolumeSLI \n"
									+ ",IFNULL( avg( Case When sol.optimization=1 and sol.total_value!=0 then sol.length * __INCH_TO_LENGTH__ end ),0 ) as TotalAvgLengthSLI \n"
									+ ",IFNULL( avg( Case When sol.optimization=1 and sol.total_value!=0 then sol.diameter * __INCH_TO_CURRENT__ end ),0 ) as TotalAvgDiameterSLI \n");
		} else {
			queryText = queryText
					.replaceAll(
							"__OPTI_SLI__",
							",IFNULL( count( Case When sol.optimization=4 then sol.id end ),0 ) as TotalNbLogsSLI \n"
									+ ",IFNULL( sum( Case When sol.optimization=4 then sol.volume* __I3_TO_CURRENT_INPUT__ end ),0 ) * __LARGE_SCALE_COEFFICIENT__ as TotalVolumeOfLogsSLI \n"
									+ ",IFNULL( FLOOR(sum( Case When sol.optimization=4 then sol.length end ) * __INCH_TO_LENGTH__),0 ) * __LARGE_SCALE_COEFFICIENT__ as TotalLinearLengthSLI \n"
									+ ",IFNULL( avg( Case When sol.optimization=4 then sol.volume* __I3_TO_CURRENT_INPUT__ end ),0 ) as TotalAvgVolumeSLI \n"
									+ ",IFNULL( avg( Case When sol.optimization=4 then sol.length * __INCH_TO_LENGTH__ end ),0 ) as TotalAvgLengthSLI \n"
									+ ",IFNULL( avg( Case When sol.optimization=4 then sol.diameter * __INCH_TO_CURRENT__ end ),0 ) as TotalAvgDiameterSLI \n");
		}
	}

	if (!this.queryText.contains("report_lugs lug")
			&& !this.queryText.contains("report_chain_displacement disp")
			&& "TagId" in params && params["TagId"] != null
			&& params["TagId"] > 0) {
		join = join
				+ " inner join tag_output t_o on t_o.output_id = sol.output_id";
		where = where + " and t_o.tag_id = " + params["TagId"];
		where_sub_batch = where_sub_batch + " and 0 = 1";
	}

	if ("OutputCSV" in params && params["OutputCSV"] != null
			&& !"".equals(params["OutputCSV"])
			&& !"null".equals(params["OutputCSV"])) {
		where = where + " and sol.output_id in (" + params["OutputCSV"] + ")";
		where_sub_batch = where_sub_batch + " and 0 = 1";
	}

	if ("Destination" in params && params["Destination"] != null
			&& !"".equals(params["Destination"])
			&& !"null".equals(params["Destination"])
			&& params["Destination"] > 0) {
		where = where + " and board.destination = " + params["Destination"];
	}
	
	if ("BoardDestination" in params && params["BoardDestination"] != null
			&& !"".equals(params["BoardDestination"])
			&& !"null".equals(params["BoardDestination"])
			&& params["BoardDestination"] >= 0
			&& params["BoardDestination"] < 999) {
		where = where + " and boards.destination = " + params["BoardDestination"];
	}
	
	if("DGGroup" in params && params["DGGroup"] != null
			&& !"".equals(params["DGGroup"])
			&& !"null".equals(params["DGGroup"])
			&& queryText.contains("downDescription")){
		where = where + " and downDescription.text_group = " + params["DGGroup"];
	}

	if ("NoLength" in params && params["NoLength"] == true) {
		queryText = queryText.replaceAll("__LENGTH_NOMINAL__", " 1 ");
		queryText = queryText.replaceAll("__PRODUCT_NAME__",
				" CONCAT(thickness.name, \"X\" , width.name) ");
	} else {

		queryText = queryText.replaceAll("__LENGTH_NOMINAL__",
				" length.nominal ");
		queryText = queryText
				.replaceAll("__PRODUCT_NAME__",
						" CONCAT(thickness.name, \"X\" , width.name, \"X\", length.name) ");
	}

	if ("Dimension_Interval" in params && params["Dimension_Interval"] != null) {
		queryText = queryText.replaceAll("__INTERVAL_TYPE__",
				params["Dimension_Interval"]);

		switch (parseInt(params["Dimension_Interval"])) {
		case 0:
			queryText = queryText.replaceAll("__INTERVAL_COL__",
					"length_interval");
			break;
		case 1:
			queryText = queryText.replaceAll("__INTERVAL_COL__",
					"width_interval");
			break;
		case 2:
			queryText = queryText.replaceAll("__INTERVAL_COL__",
					"thickness_interval");
			break;
		case 3:
			queryText = queryText
					.replaceAll("__INTERVAL_COL__", "gap_interval");
			break;
		case 4:
			queryText = queryText.replaceAll("__INTERVAL_COL__",
					"diameter_interval");
			break;
		case 5:
			queryText = queryText.replaceAll("__INTERVAL_COL__",
					"moisture_interval");
			break;
		case 6:
			queryText = queryText.replaceAll("__INTERVAL_COL__",
					"crook_interval");
			break;
		case 7:
			queryText = queryText.replaceAll("__INTERVAL_COL__",
					"twist_interval");
			break;
		case 8:
			queryText = queryText
					.replaceAll("__INTERVAL_COL__", "bow_interval");
			break;

		}
	}

	if ("Dimension_Raw" in params && params["Dimension_Raw"] != null) {
		switch (parseInt(params["Dimension_Raw"])) {
		case 0:
			queryText = queryText.replaceAll("__RAW_DATA_COL__", "length");
			/*
			 * Permet de diviser les longueur en pied/Metre au lieu de les
			 * garder en pouces/cm
			 */
			if ("metric" in params && params["metric"] == true) {
				queryText = queryText.replaceAll("__RAW_DATA_DIVISER__",
						"1/100");
			} else {
				queryText = queryText
						.replaceAll("__RAW_DATA_DIVISER__", "1/12");
			}
			break;
		case 1:
			queryText = queryText.replaceAll("__RAW_DATA_COL__", "width");
			queryText = queryText.replaceAll("__RAW_DATA_DIVISER__", "1");
			break;
		case 2:
			queryText = queryText.replaceAll("__RAW_DATA_COL__", "thickness");
			queryText = queryText.replaceAll("__RAW_DATA_DIVISER__", "1");
			break;
		case 3:
			queryText = queryText.replaceAll("__RAW_DATA_COL__", "gap");
			queryText = queryText.replaceAll("__RAW_DATA_DIVISER__", "1");
			break;
		case 4:
			queryText = queryText.replaceAll("__RAW_DATA_COL__", "diameter");
			queryText = queryText.replaceAll("__RAW_DATA_DIVISER__", "1");
			break;
		case 5:
			queryText = queryText.replaceAll("__RAW_DATA_COL__", "moisture");
			queryText = queryText.replaceAll("__RAW_DATA_DIVISER__", "1");
			break;

		}
	}

	if ("IntervalType" in params && params["IntervalType"] != null) {
		queryText = queryText.replaceAll("__INTERVAL_TYPE__",
				params["IntervalType"]);

		switch (parseInt(params["IntervalType"])) {
		case 0:
			queryText = queryText.replaceAll("__INTERVAL_COL__",
					"length_interval");
			queryText = queryText.replaceAll("__RAW_DATA_COL__", "length");
			break;
		case 1:
			queryText = queryText.replaceAll("__INTERVAL_COL__",
					"width_interval");
			queryText = queryText.replaceAll("__RAW_DATA_COL__", "width");
			break;
		case 2:
			queryText = queryText.replaceAll("__INTERVAL_COL__",
					"thickness_interval");
			queryText = queryText.replaceAll("__RAW_DATA_COL__", "thickness");
			break;
		case 3:
			queryText = queryText
					.replaceAll("__INTERVAL_COL__", "gap_interval");
			queryText = queryText.replaceAll("__RAW_DATA_COL__", "line_gap");
			break;
		case 4:
			queryText = queryText.replaceAll("__INTERVAL_COL__",
					"diameter_interval");
			queryText = queryText.replaceAll("__RAW_DATA_COL__", "diameter");
			break;
		case 5:
			queryText = queryText.replaceAll("__INTERVAL_COL__",
					"moisture_interval");
			queryText = queryText.replaceAll("__RAW_DATA_COL__",
					"average_moisture");
			break;
		}
	}

	if ("LengthInterval" in params && params["LengthInterval"]
			&& params["LengthInterval"] != 0) {
		where = where + " and sol.length_interval = "
				+ params["LengthInterval"];
	}

	if ("Data" in params && params["Data"] != null) {
		if (params["Data"] == 0) {
			queryText = queryText.replaceAll("__DATA_TO_SELECT__",
					"count(sol.id)");

			queryText = queryText.replaceAll("__BOARD_DATA_TO_SELECT__",
					"count(boards.id)");

			queryText = queryText.replaceAll("__CONDITION_NUMBER_VOLUME__",
					"then 1 else 0");
		} else if (params["Data"] == 1) {
			queryText = queryText
					.replaceAll("__DATA_TO_SELECT__",
							"sum(sol.volume * __I3_TO_CURRENT_INPUT__ * __LARGE_SCALE_COEFFICIENT__ )");

			queryText = queryText
					.replaceAll("__BOARD_DATA_TO_SELECT__",
							"sum(thickness.nominal * width.nominal * length.nominal) * __I3_TO_CURRENT__ ");

			queryText = queryText
					.replaceAll(
							"__CONDITION_NUMBER_VOLUME__",
							"then sol.volume * __I3_TO_CURRENT_INPUT__ * __LARGE_SCALE_COEFFICIENT__  else 0");
		}
	}

	if ("CombinationTrimBlocks" in params
			&& params["CombinationTrimBlocks"] == 1) {
		queryText = queryText
				.replaceAll("__TRIM_BLOCKS_COMBINATION__",
						"combination.name, IFNULL(concat(' ', trim_block_combination.name), '')");
		join = join
				+ " left join report_combination trim_block_combination on sol.trim_block_combination = trim_block_combination.id"
	} else {
		queryText = queryText.replaceAll("__TRIM_BLOCKS_COMBINATION__",
				"combination.name");
	}

	if ("input-metric" in params && params["input-metric"] == true) {
		queryText = queryText.replaceAll("__I3_TO_CURRENT_INPUT__",
				"1/(144 * 423.7)");
	} else {
		queryText = queryText.replaceAll("__I3_TO_CURRENT_INPUT__", "1/(1728)");
	}

	if ("Interval" in params && params["Interval"] != null) {
		where = where + " and i.id = " + params["Interval"];
	}

	if ("SupervisorId" in params && params["SupervisorId"] != null
			&& params["SupervisorId"] > 0) {
		join = join + " inner join report_shift shift on shift.id = sol.shift";
		join = join
				+ " inner join report_shift_definition shiftDef  on shiftDef.id = shift.shift_definition";
		join = join
				+ " inner join report_work_team workTeam on workTeam.id = shiftDef.work_team";
		where = where + " and workTeam.supervisor = " + params["SupervisorId"];
	}

	if ("LargeScaleCoefficient" in params
			&& params["LargeScaleCoefficient"].value
			&& (params["input-metric"] == null || !params["input-metric"].value)) {
		queryText = queryText.replaceAll("__LARGE_SCALE_COEFFICIENT__",
				"(1 / 1000)");
	} else {
		queryText = queryText.replaceAll("__LARGE_SCALE_COEFFICIENT__", "1");
	}

	if ("metric" in params && params["metric"] == true) {
		queryText = queryText.replaceAll("__I3_TO_CURRENT__",
				"(1 / (144 * 423.7))");
		queryText = queryText.replaceAll("__INCH_TO_CURRENT__", "(2.54)");
		queryText = queryText.replaceAll("__INCH_TO_LENGTH__", "(2.54)/100");
		queryText = queryText.replaceAll("__INCH_TO_CURRENT_MM__",
				"(2.54 * 10)");
		queryText = queryText.replaceAll("__LENGTH_TO_CURRENT__", "0.3048");
		queryText = queryText.replaceAll("__SWEEP_TO_CURRENT__", "25.4");
		/* queryText = queryText.replaceAll("__SWEEP_TO_CURRENT__", "3.175"); */
		queryText = queryText.replaceAll("__SWEEP_INTERVAL__", "1");
		queryText = queryText.replaceAll("__TAPER_TO_CURRENT__", "1000");
		queryText = queryText.replaceAll("__TAPER_INTERVAL__", "1");
		queryText = queryText.replaceAll("__TAPER_LENGTH_TO_CURRENT__",
				"39.3700788");
		queryText = queryText.replaceAll("__CUT_INTERVAL__", "10");
		queryText = queryText.replaceAll("__DENSITY_TO_CURRENT__", "1")
	} else {
		queryText = queryText.replaceAll("__I3_TO_CURRENT__", "(1 / 144)");
		queryText = queryText.replaceAll("__INCH_TO_CURRENT__", "1");
		queryText = queryText.replaceAll("__INCH_TO_CURRENT_MM__", "1");
		queryText = queryText.replaceAll("__INCH_TO_LENGTH__", "1/12");
		queryText = queryText.replaceAll("__LENGTH_TO_CURRENT__", "1");
		queryText = queryText.replaceAll("__SWEEP_TO_CURRENT__", "1");
		queryText = queryText.replaceAll("__SWEEP_INTERVAL__", ".25");
		queryText = queryText.replaceAll("__TAPER_TO_CURRENT__", "12");
		queryText = queryText.replaceAll("__TAPER_LENGTH_TO_CURRENT__", "12");
		queryText = queryText.replaceAll("__TAPER_INTERVAL__", ".025");
		queryText = queryText.replaceAll("__DENSITY_TO_CURRENT__", "1");

		if ("Dimension_Raw" in params && params["Dimension_Raw"] != null
				&& params["Dimension_Raw"] == 0) {
			queryText = queryText.replaceAll("__CUT_INTERVAL__", "12");
		} else {
			queryText = queryText.replaceAll("__CUT_INTERVAL__", "10");
		}
	}

	if ("MachineName" in params) {
		queryText = queryText.replaceAll("_machine_", "_"
				+ params["MachineName"] + "_");
	}
	if ("DowngradeTypes" in params) {
		if (params["DowngradeTypes"] == 1) {
			queryText = queryText.replaceAll("__DOWNGRADE_PRODUCT_ID__",
					"downgrade_unique_product");
			queryText = queryText.replaceAll("__DOWNGRADE_CAUSE_CODE__",
					"down_grade_cause_unique");
		} else if (params["DowngradeTypes"] == 2) {
			queryText = queryText.replaceAll("__DOWNGRADE_PRODUCT_ID__",
					"downtrim_unique_product");
			queryText = queryText.replaceAll("__DOWNGRADE_CAUSE_CODE__",
					"trim_cause_unique");
		}
	}

	if ("GroupBy" in params && params["GroupBy"] != null) {
		if (params["GroupBy"] == 0) {
			queryText = queryText.replaceAll("__GROUP_BY_NAME__",
					"IFNULL(inter.name, \""
							+ reportContext.getMessage("Interval",
									reportContext.getLocale()) + "\")");
			join = join
					+ " left join report_interval inter on sol.diameter_interval = inter.id ";
		} else if (params["GroupBy"] == 1) {

			queryText = queryText.replaceAll("__GROUP_BY_NAME__",
					"category.name");
			join = join
					+ " inner join report_category category on sol.category = category.id  ";
		} else {
			queryText = queryText.replaceAll("__GROUP_BY_NAME__", "cut.name");
			join = join
					+ " inner join report_category category on sol.category = category.id  ";
			join = join
					+ " inner join report_cutting_pattern cut on category.cutting_pattern = cut.id  ";
		}
	}

	if ("SqlFragment" in params && params["SqlFragment"] != null
			&& params["SqlFragment"] != "") {
		if (!this.queryText.contains("report_lugs lug")
				&& !this.queryText.contains("report_chain_displacement disp")) {
			where = where + " AND " + params["SqlFragment"];
		}
	}


	queryText = queryText.replaceAll("__WHERE__", where);
	queryText = queryText.replaceAll("__WHERE_SUB_BATCH__", where_sub_batch);
	queryText = queryText.replaceAll("__WHERE_BATCH_REPORT__",
			where_batch_report);
	queryText = queryText.replaceAll("__WHERE_DOWNTIME__", where_downtime);

	queryText = queryText.replaceAll("__JOINS__", join);
	return queryText;
}